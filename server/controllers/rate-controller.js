import express from 'express';
import rateService from '../services/rate-service.js';
import rateData from '../data/rate-data.js';
import photoService from '../services/photo-service.js';
import photoData from '../data/photo-data.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import userService from '../services/user-service.js';
import userData from '../data/user-data.js';

const rateController = express.Router();

rateController.post('/:contestId/rates/photo/:photoId', authMiddleware, roleMiddleware('organiser'), async (req, res) => {
    const { contestId, photoId } = req.params;
    const { score, comment, checkbox, juryId } = req.body;

    const doesContestExist = await photoService.getContest(photoData)(contestId);

    if (!doesContestExist) {
        return res.status(404).json({ message: "Contest not found!" });
    }

    const photo = await photoService.getPhoto(photoData)(photoId, contestId);

    if (!photo) {
        return res.status(404).json({ message: 'Photo not found!' });
    }

    const isPhase2 = await rateService.getPhase(rateData)(contestId);

    if (isPhase2.error) {
        return res.status(400).json({ message: isPhase2.error });
    }

    const juryRate = await rateService.getJuryRate(rateData)(juryId, photoId, contestId);

    if (juryRate.error) {
        return res.status(400).json({ message: juryRate.error });
    }

    const rate = {
        photoId: photoId,
        juryId: juryRate.juryId,
        contestId: contestId,
        score: score,
        comment: comment,
        checkbox: checkbox,
    }

    await rateService.ratePhoto(rateData)(rate);
    res.status(200).json({ message: 'Photo successfully rated!', rate: rate });
})

rateController.get('/:contestId/rates/:userId/photo/:photoId', authMiddleware, async (req, res) => {
    const { contestId, userId, photoId } = req.params;

    const doesContestExist = await photoService.getContest(photoData)(contestId);

    if (!doesContestExist) {
        return res.status(404).json({ message: "Contest not found!" });
    }

    const user = await userService.getUser(userData)('id', userId);

    if (!user) {
        return res.status(404).json({ message: 'User not found!' });
    }

    const photo = await photoService.getPhoto(photoData)(photoId, contestId);

    if (!photo) {
        return res.status(404).json({ message: 'Photo not found!' });
    }

    const getRateAndComment = await rateService.getRateAndComment(rateData)(contestId, userId, photoId);

    res.status(200).json(getRateAndComment);
})

rateController.get('/:contestId/rates', authMiddleware, roleMiddleware('user'), async (req, res) => {
    const { contestId } = req.params;

    const doesContestExist = await photoService.getContest(photoData)(contestId);

    if (!doesContestExist) {
        return res.status(404).json({ message: "Contest not found!" });
    }
    // IS CONTEST FINISHED ?
    const photosByOtherUsers = await rateService.getPhotoByOtherUsers(rateData)(contestId);

    if (!photosByOtherUsers) {
        return res.status(404).json({ message: "There are no ratings!" });
    }

    res.status(200).json(photosByOtherUsers);
})

rateController.get('/:contestId/rates/user/:userId/photo/:photoId', authMiddleware, async (req, res) => {
    const { contestId, userId, photoId } = req.params;

    const doesContestExist = await photoService.getContest(photoData)(contestId);

    if (!doesContestExist) {
        return res.status(404).json({ message: "Contest not found!" });
    }

    const user = await userService.getUser(userData)('id', userId);

    if (!user) {
        return res.status(404).json({ message: 'User not found!' });
    }

    const photo = await photoService.getPhoto(photoData)(photoId, contestId);

    if (!photo) {
        return res.status(404).json({ message: 'Photo not found!' });
    }

    const photoRates = await rateService.getPhotoRates(rateData)(contestId, photoId);

    if (photoRates.error) {
        return res.status(400).json({ message: photoRates.error });
    }

    res.status(200).json(photoRates.rates);
})

export default rateController;