import express from 'express';
import photoService from '../services/photo-service.js';
import photoData from '../data/photo-data.js';
import bodyValidator from '../validators/body-validator.js';
import { uploadPhotoSchema } from '../validators/upload-photo.js';
import userService from '../services/user-service.js';
import userData from '../data/user-data.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';

const photoController = express.Router();

photoController

    .get('/:contestId/photos', authMiddleware, roleMiddleware('organiser'), async (req, res) => {
        const { contestId } = req.params;

        const doesContestExist = await photoService.getContest(photoData)(contestId);

        if (!doesContestExist) {
            return res.status(404).json({ message: "Contest not found!" });
        }

        const photos = await photoService.getAllPhotosFromContest(photoData)(contestId);

        if (photos.error) {
            return res.status(404).json({ message: photos.error });
        }

        res.status(200).json(photos.photos);
    })

    .get('/:contestId/photos/:photoId', authMiddleware, async (req, res) => { // Custom Middleware ??
        const { contestId, photoId } = req.params;

        const doesContestExist = await photoService.getContest(photoData)(contestId);

        if (!doesContestExist) {
            return res.status(404).json({ message: "Contest not found!" });
        }

        const photo = await photoService.getPhoto(photoData)(photoId, contestId);

        if (!photo) {
            return res.status(404).json({ message: 'Photo not found!' });
        }

        res.status(200).json(photo);
    })

    .get('/:contestId/photo/user/:userId', authMiddleware, roleMiddleware('user'), async (req, res) => {
        const { contestId, userId } = req.params;

        const doesContestExist = await photoService.getContest(photoData)(contestId);

        if (!doesContestExist) {
            return res.status(404).json({ message: "Contest not found!" });
        }

        const userPhoto = await photoService.getUserPhoto(photoData)(userId, contestId);

        if (!userPhoto.error) {
            return res.status(200).json({ message: `User has not uploaded photo for this contest` });
        }

        res.status(200).json(userPhoto);
    })

    .post('/:contestId/photos', authMiddleware, roleMiddleware('user'), bodyValidator('photoInfo', uploadPhotoSchema), async (req, res) => {
        const { contestId } = req.params;
        const { story, title, image, userId } = req.body;

        const doesContestExist = await photoService.getContest(photoData)(contestId);

        if (!doesContestExist) {
            return res.status(404).json({ message: "Contest not found!" });
        }

        const doesUserExist = await userService.getUser(userData)('id', userId);

        if (!doesUserExist) {
            return res.status(404).json({ message: 'User not found!' });
        }

        const userAlreadyHasPhoto = await photoService.getUserPhoto(photoData)(userId, contestId);

        if (userAlreadyHasPhoto.error) {
            return res.status(400).json({ message: userAlreadyHasPhoto.error });
        }

        const photoInfo = {
            contestId: contestId,
            story: story,
            title: title,
            image: image,
            userId: userId,
            endPhase1Time: doesContestExist.endPhase1Time
        };

        const uploadedPhoto = await photoService.uploadPhoto(photoData)(photoInfo);

        if (uploadedPhoto.error) {
            return res.status(403).json({ message: uploadedPhoto.error });
        }

        res.status(200).json({ message: "Photo uploaded!", uploadedPhoto: uploadedPhoto.photo });
    });

export default photoController;