import express from 'express';
import contestService from '../services/contest-service.js';
import contestData from '../data/contest-data.js';
import userService from '../services/user-service.js';
import userData from '../data/user-data.js';
import { createContestSchema } from '../validators/create-contest.js';
import bodyValidator from '../validators/body-validator.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import photoService from '../services/photo-service.js';
import photoData from '../data/photo-data.js';

const contestController = express.Router();

contestController.post('/',
    authMiddleware,
    roleMiddleware('organiser'),
    bodyValidator('contest', createContestSchema),
    async (req, res) => {
        const contest = {
            title: req.body.title,
            category: req.body.category,
            type: req.body.type,
            endPhase1Time: req.body.endPhase1Time,
            endPhase2Time: req.body.endPhase2Time,
            listOfUsers: req.body.listOfUsers,
            usersAsJury: req.body.usersAsJury,
        };
        const createdContest = await contestService.createContest(contestData)(contest);

        if (createdContest.error) {
            return res.status(400).json({ message: createdContest.error });
        }
        if (contest.type === 'invitational') {
            const addListOfUsers = await contestService.insertUsersToContest(contestData)(contest.listOfUsers, contest.usersAsJury, createdContest.contest.id);
        }
        const insertJury = await contestService.insertJury(contestData)(createdContest.contest.id);

        res.status(201).json(createdContest);
    });

contestController.post('/join/:contestId/users/:userId', authMiddleware, roleMiddleware('user'), async (req, res) => {
    const { userId, contestId } = req.params;

    const enrollInContest = {
        userId: userId,
        contestId: contestId,
    }

    const doesContestExist = await photoService.getContest(photoData)(contestId);

    if (!doesContestExist) {
        return res.status(404).json({ message: "Contest not found!" });
    }

    const doesUserExist = await userService.getUser(userData)('id', userId);

    if (!doesUserExist) {
        return res.status(404).json({ message: 'User not found!' });
    }

    const userAlreadyJoined = await contestService.alreadyJoined(contestData)(userId, contestId);

    if (userAlreadyJoined.error) {
        return res.status(400).json({ message: userAlreadyJoined.error, userJoined: userAlreadyJoined.userJoined });
    }

    const enroll = await contestService.joinTheContest(contestData)(enrollInContest);

    if (enroll.error) {
        return res.status(403).json({ message: enroll.error });
    }
    res.status(201).json({ message: "Successfully enrolled!", userJoined: +contestId });
})

contestController.get('/phase1', authMiddleware, roleMiddleware('organiser'), async (req, res) => {
    const foundContests = await contestService.contestPhase1(contestData)();

    if (foundContests.length === 0) {
        res.status(400).json({ error: 'No contests in phase one found!' });
    } else {
        res.status(200).json(foundContests);
    }
});

contestController.get('/phase2', authMiddleware, roleMiddleware('organiser'), async (req, res) => {
    const foundContests = await contestService.contestPhase2(contestData)();

    if (foundContests.length === 0) {
        res.status(400).json({ message: 'No contests in phase two found!' });
    } else {
        res.status(200).json(foundContests);
    }
});

contestController.get('/finished', authMiddleware, roleMiddleware('organiser'), async (req, res) => {
    const foundContests = await contestService.contestFinished(contestData)();

    if (foundContests.length === 0) {
        res.status(400).json({ message: 'No contests in phase finished found!' });
    } else {
        res.status(200).json(foundContests);
    }
})

contestController.get('/open', authMiddleware, roleMiddleware('user'), async (req, res) => {
    const foundContests = await contestService.contestOpen(contestData)();

    if (foundContests.error) {
        res.status(400).json({ message: foundContests.error });
    } else {
        res.status(200).json(foundContests.contests);
    }
})

contestController.get('/open/users/:userId', authMiddleware, roleMiddleware('user'), async (req, res) => {
    const user = await userService.getUser(userData)('id', req.params.userId);

    if (!user) {
        return res.status(404).json({ message: 'User not found!' });
    }

    const foundContests = await contestService.contestUser(contestData)(req.params.userId);

    if (foundContests.error) {
        return res.status(404).json({ message: foundContests.error });
    }

    return res.status(200).json(foundContests);
})

contestController.get('/users/:userId', authMiddleware, roleMiddleware('user'), async (req, res) => {
    const user = await userService.getUser(userData)('id', req.params.userId);

    if (!user) {
        return res.status(404).json({ message: 'User not found!' });
    }

    const userEndedContests = await contestService.getUserEndedContests(contestData)(req.params.userId);

    return res.status(200).json(userEndedContests);
})

export default contestController;