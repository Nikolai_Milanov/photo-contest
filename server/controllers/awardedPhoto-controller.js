import express from 'express';
import awardedPhotoService from '../services/awardedPhoto-service.js';
import awardedPhotoData from '../data/awardedPhoto-data.js';
import photoService from '../services/photo-service.js';
import photoData from '../data/photo-data.js';
import { authMiddleware } from '../auth/auth-middleware.js';

const awardedPhotoController = express.Router();

awardedPhotoController
    /**
     * Retrieves all awarded photos from contests route (end point).
     * Gives response for all the awarded photos from all contests.
     */
    .get('/awardedPhotos', authMiddleware, async (req, res) => {
        const photos = await awardedPhotoService.getAllAwardedPhotosFromContests(awardedPhotoData)();

        if (photos.length === 0) {
            return res.status(404).json({ message: 'There are no awarded photos!' });
        }

        res.status(200).json(photos);
    })

    /**
     * View individual awarded photo route (end point).
     * Gives response with the awarded photo that is requested.
     */
    .get('/:contestId/awardedPhotos/:photoId', authMiddleware, async (req, res) => {
        const { contestId, photoId } = req.params;

        const doesContestExist = await photoService.getContest(photoData)(contestId);

        if (!doesContestExist) {
            return res.status(404).json({ message: "Contest not found!" });
        }

        const photo = await photoService.getPhoto(photoData)(photoId, contestId);

        if (!photo) {
            return res.status(404).json({ message: 'Photo not found!'});
        }

        const isPhotoAwarded = await awardedPhotoService.getPhoto(awardedPhotoData)(photoId);

        if (isPhotoAwarded.error) {
            return res.status(404).json({ message: isPhotoAwarded.error });
        }

        res.status(200).json(isPhotoAwarded.photo);
    });

export default awardedPhotoController;