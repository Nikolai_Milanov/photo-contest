import express from 'express';
import userService from '../services/user-service.js';
import userData from '../data/user-data.js';
import serviceErrors from '../services/service-errors.js';
import { createUserSchema } from '../validators/create-user.js';
import bodyValidator from '../validators/body-validator.js';
import createToken from '../auth/create-token.js';
import { roleMiddleware, authMiddleware } from '../auth/auth-middleware.js';

const userController = express.Router();

userController

    .post('/register', bodyValidator('user', createUserSchema), async (req, res) => {
        const { username, firstName, lastName, password } = req.body;

        const foundName = await userService.getUser(userData)('username', username);

        if (foundName) {
            return res.status(403).json({ message: serviceErrors.DUPLICATE_RECORD });
        }

        const user = await userService.regUser(userData)(username, firstName, lastName, password);
        if (!user) {
            return res.status(400).json({ message: 'User not created!' });
        }
        res.status(201).json({ message: 'User successfully created!', user: user });
    })

    .post('/session', async (req, res) => {
        const { username, password } = req.body;

        const foundName = await userService.getUser(userData)('username', username);

        if (!foundName) {
            return res.status(400).json({ message: 'Incorrect username!' });
        }

        const isPasswordValid = await userService.getHashedPassword(userData)(username, password);

        if (!isPasswordValid) {
            return res.status(400).json({ message: 'Incorrect password!' });
        }

        const payload = {
            sub: foundName.id,
            username: foundName.username,
            role: foundName.role
        }

        const token = createToken(payload);

        res.status(200).json({ message: 'You Logged in!', token });
    })

    .delete('/session', async (req, res) => {
        res.status(200).json({ message: 'Successfully logged out!' });
    })

    .get('/users', authMiddleware, roleMiddleware('organiser'), async (req, res) => {
        const foundUsers = await userService.getAllUsers(userData)();

        if (foundUsers.length === 0) {
            res.status(404).json({ message: serviceErrors.RECORD_NOT_FOUND })
        } else {
            res.status(200).json(foundUsers);
        }
    })
    .get('/user/:userId', authMiddleware, roleMiddleware('user'), async (req, res) => {
        const foundUser = await userService.getUser(userData)('id', req.params.userId);

        if (foundUser.length === 0) {
            res.status(404).json({ message: serviceErrors.RECORD_NOT_FOUND })
        } else {
            res.status(200).json(foundUser);
        }
    })


export default userController;