import pool from './pool.js';

const getContest = async (contestId) => {
    const sql = `
        SELECT id, title, category, type, endPhase1Time, endPhase2Time, createdOn, phase1Duration, phase2Duration
        FROM contest
        WHERE contest.id = ?;
    `;

    const result = await pool.query(sql, [contestId]);
    return result[0];
}

const getAllPhotosFromContest = async (contestId) => {
    const sql = `
        SELECT id, story, title, image, user_id, contest_id
        FROM photo
        WHERE contest_id = ?;
    `;

    return await pool.query(sql, [contestId]);
}

const getPhoto = async (photoId, contestId) => {
    const sql = `
        SELECT id, story, title, image, user_id, contest_id
        FROM photo
        WHERE id = ? AND contest_id = ?;
    `;

    const result = await pool.query(sql, [photoId, contestId]);
    return result[0];
}

const getUserPhoto = async (userId, id) => {
    const sql = `
        SELECT photo.id, photo.story, photo.title, photo.image
        FROM photo
        JOIN contest
        ON photo.contest_id = contest.id
        JOIN user
        ON photo.user_id = user.id
        WHERE user.id = ? and 
        contest.id = ?;
    `;

    const result = await pool.query(sql, [userId, id]);
    return result[0];
}

const uploadPhoto = async ({ story, title, image, userId, contestId }) => {
    const sql = `
        INSERT INTO photo (story, title, image, user_id, contest_id)
        VALUES (?, ?, ?, ?, ?);
    `;

    return await pool.query(sql, [
        story,
        title,
        image,
        userId,
        contestId
    ]);
}

export default {
    getContest,
    getAllPhotosFromContest,
    getPhoto,
    getUserPhoto,
    uploadPhoto
};