import pool from './pool.js';

const getUser = async (column, value) => {
    const sql = `
        SELECT id, username, firstName, lastName, points, role
        FROM user
        WHERE ${column} = ?;
    `;

    const result = await pool.query(sql, [value]);
    return result[0];
}

const getAllUsers = async () => {
    const sql = `
    SELECT u.id, u.username, u.firstName, u.lastName, u.points, u.role
    FROM user as u
    WHERE u.role = 'user'
    ORDER BY points DESC;
    `;

    const result = await pool.query(sql);
    return result;
}

const getHashedPassword = async (column, value) => {
    const sql = `
        SELECT password
        FROM user
        WHERE ${column} = ?;
    `;

    const result = await pool.query(sql, [value]);
    return result[0];
}

const regUser = async (username, firstName, lastName, password) => {
    const sql = `
        INSERT INTO user (username, firstName, lastName, password)
        VALUES (?, ?, ?, ?);
    `;

    return await pool.query(sql, [username, firstName, lastName, password]);
}

export default {
    getUser,
    getHashedPassword,
    regUser,
    getAllUsers
}