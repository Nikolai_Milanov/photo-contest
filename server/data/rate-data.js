import pool from './pool.js';

const getJuryRate = async (juryId, photoId) => {
    const sql = `
    SELECT score, comment, photo_id, jury_id FROM rate
    JOIN photo on rate.photo_id = photo.id
    JOIN jury on rate.jury_id = jury.id
    WHERE jury.id = ? and 
    photo.id = ?;
    `;

    const result = await pool.query(sql, [juryId, photoId]);
    return result[0];
}

const getJuryById = async (juryId, contestId) => {
    const sql = `
    SELECT id, user_id, contest_id FROM jury
    WHERE user_id = ?
    AND contest_id = ?;
    `
    const result = await pool.query(sql, [juryId, contestId]);
    return result[0];
}

const getPhase = async (contestId) => {
    const sql = `
    SELECT endPhase1Time, endPhase2Time from contest
    WHERE contest.id = ?;
    `;

    const result =  await pool.query(sql, [contestId]);
    return result[0];
}

const ratePhoto = async ({ score, comment, photoId, juryId }) => {
    const sql = `
    INSERT INTO rate (score, comment, photo_id, jury_id)
        VALUES (?, ?, ?, ?);
    `;

    return await pool.query(sql, [score, comment, photoId, juryId])
}

const getRateAndComment = async (contestId, userId, photoId) => {
    const sql = `
    SELECT rate.id, rate.score, rate.comment from rate
    JOIN photo on rate.photo_id = photo.id
    JOIN contest on photo.contest_id = contest.id
    JOIN user on photo.user_id = user.id
    JOIN jury on rate.jury_id = jury.id
    WHERE contest.id = ?
    AND jury.user_id = ?
    AND photo.id = ?;
    `;

    return await pool.query(sql, [contestId, userId, photoId]);
}

const getPhotoRates = async (contestId, photoId) => {
    const sql = `
    SELECT rate.id, rate.score, rate.comment from rate
    JOIN photo on rate.photo_id = photo.id
    JOIN contest on photo.contest_id = contest.id
    JOIN user on photo.user_id = user.id
    WHERE contest.id = ?
    AND photo.id = ?;
    `;

    return await pool.query(sql, [contestId, photoId]);
}

const getPhotoByOtherUsers = async (contestId) => {
    const sql = `
    SELECT score, comment, photo.image from rate
    JOIN photo on rate.photo_id = photo.id
    JOIN contest on contest_id = contest.id
    WHERE contest.id = ?
    `;

    const result =  await pool.query(sql, [contestId]);
    return result[0];
}

export default {
    getJuryRate,
    getPhase,
    ratePhoto,
    getJuryById,
    getRateAndComment,
    getPhotoRates,
    getPhotoByOtherUsers,
}