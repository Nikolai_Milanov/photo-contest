import pool from './pool.js';

/**
 * Retrieves a collection of awarded photos stored in the database.
 * @returns {Array} array of objects with awarded photos data.
 */
const getAllAwardedPhotosFromContests = async () => {
    const sql = `
        SELECT id, contest_id, photo_id, awardedDate
        FROM awarded_photo;
    `;

    const result = await pool.query(sql);
    return result;
}

/**
 * Retrieves an awarded photo stored in the database.
 * @returns {Object} object with the awarded photo data.
 */
const getPhoto = async (photoId) => {
    const sql = `
        SELECT id, contest_id, photo_id, awardedDate
        FROM awarded_photo
        WHERE photo_id = ?;
    `;

    const result = await pool.query(sql, [photoId]);
    return result[0];
}

/**
 * Updates all contests that are finished in the database.
 * @returns {Object} object with information for the response of the update request.
 */
const endedContests = async () => {
    const sql = `
        UPDATE contest
        SET contestDone = 1
        WHERE endPhase2Time < CURRENT_TIMESTAMP()
        AND contestDone = 0;
    `;

    const result = await pool.query(sql);
    return result;
}

/**
 * Retrieves the id for every finished contest that not exists in the awarded photo entity.
 * @returns {Array} array of objects with every finished contest that hasn't set an awarded photo yet.
 */
const getCompletedContests = async () => {
    const sql = `
        SELECT id
        FROM contest
        WHERE NOT EXISTS (SELECT contest_id FROM awarded_photo)
        AND contestDone = 1;
    `;

    const result = await pool.query(sql);
    return result;
}

/**
 * Retrieves all the photos from a contest.
 * @param {Number} number  - the contest's id.
 * @returns {Array} array of objects with the photos data from the contest.
 */
const getPhotosToAward = async (contestId) => {
    const sql = `
        SELECT p.id, p.contest_id as contestId
        FROM photo p
        JOIN contest c
        ON p.contest_id = c.id
        WHERE c.id = ?;
    `;

    const result = await pool.query(sql, [contestId]);
    return result;
}

/**
 * Retrieves the rates of every photo from a contest.
 * @param {Number} number - the photo's id.
 * @param {Number} number - the contest's id.
 * @returns {Array} array of objects with the average rate of every photo from the contest.
 */
const getRatesFromPhoto = async (photoId, contestId) => {
    const sql = `
        SELECT AVG(r.score) AS score, p.id AS photoId, c.id AS contestId
        FROM rate r
        JOIN photo p
        ON r.photo_id = p.id
        JOIN contest c
        ON p.contest_id = c.id
        WHERE p.id = ?
        AND c.id = ?;
    `;

    const result = await pool.query(sql, [photoId, contestId]);
    return result;
}

/**
 * Adds an awarded photo from a contest to awarded photo entity in the database.
 * @param {Number} number - the photo's id.
 * @returns {Object} object with information for the response of the insert request.
 */
const setAwardedPhotos = async (photoId) => {
    const sql = `
        INSERT INTO awarded_photo (contest_id, photo_id, awardedDate)
        SELECT c.id, p.id, c.endPhase2Time
        FROM photo p
        JOIN contest c
        ON c.id = p.contest_id
        WHERE p.id = ?;
    `;

    return await pool.query(sql, [photoId]);
}

/**
 * Retrieves the type of a contest.
 * @param {Number} number - the contest's id.
 * @returns {Object} object with the type of the contest ('open' or 'invitational').
 */
const typeOfContest = async (contestId) => {
    const sql = `
        SELECT type
        FROM contest
        WHERE id = ?;
    `;

    const result = await pool.query(sql, [contestId]);
    return result[0];
}

/**
 * Updates the points of a user with a photo that is in top 3 of the finished contest.
 * @param {Number} number - points that are added to the user's points.
 * @param {Number} number - the photo's id.
 * @returns {Object} object with information for the response of the update request.
 */
const setUserPoints = async (points, photoId) => {
    const sql = `
        UPDATE user
        JOIN photo
        ON photo.user_id = user.id
        SET user.points = user.points+?
        WHERE photo.id = ?;
    `;

    return await pool.query(sql, [points, photoId]);
}

export default {
    getAllAwardedPhotosFromContests,
    getPhoto,
    endedContests,
    getCompletedContests,
    getPhotosToAward,
    getRatesFromPhoto,
    setAwardedPhotos,
    typeOfContest,
    setUserPoints
}