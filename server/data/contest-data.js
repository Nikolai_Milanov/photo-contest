import pool from './pool.js';

const createContest = async ({title, category, type, formatedPhase1, formatedPhase2, createdOn, phase1Duration, phase2Duration}) => {
  const sql = `
    insert into contest (title, category, type, endPhase1Time, endPhase2Time, createdOn, phase1Duration, phase2Duration, contestDone)
    values (?, ?, ?, ?, ?, ?, ?, ?, 0);
    `;

  const result = await pool.query(sql, [title, category, type, formatedPhase1, formatedPhase2, createdOn, phase1Duration, phase2Duration]);
  return result;
};

const insertJury = async (organiserId, contestId) => {
  const sql = `
  INSERT INTO jury (user_id, contest_id)
  values (?, ?);
  `;

  const result = await pool.query(sql, [organiserId, contestId]);
  return result;
}

const joinTheContest = async ({ userId, contestId }) => {
  const sql = `
  INSERT INTO user_has_contest (user_id, contest_id)
  values (?, ?);
  `;

  return await pool.query(sql, [userId, contestId]);
}

const alreadyJoined = async (userId, contestId) => {
  const sql = `
  SELECT user_id, contest_id 
  FROM user_has_contest
  WHERE user_id = ? and
  contest_id = ?;
  `;

  const result = await pool.query(sql, [userId, contestId]);
  return result[0];
}

const contestPhase1 = async () => {
  const sql = `
    SELECT * 
    FROM contest
    WHERE endPhase1Time > CURRENT_TIMESTAMP();
    `;

    const result = await pool.query(sql);
    return result;
};

const contestPhase2 = async () => {
    const sql = `
    SELECT * 
    FROM contest
    WHERE endPhase1Time < CURRENT_TIMESTAMP() and
    endPhase2Time > CURRENT_TIMESTAMP();
    `;

    const result = await pool.query(sql);
    return result;
}

const contestFinished = async () => {
    const sql = `
    SELECT * 
    FROM contest
    WHERE endPhase1Time < CURRENT_TIMESTAMP() and
    endPhase2Time < CURRENT_TIMESTAMP();
    `;

    const result = await pool.query(sql);
    return result; 
}

const contestOpen = async () => {
  const sql =`
  SELECT id, title, category, type, endPhase1Time, endPhase2Time, createdOn, phase1Duration, phase2Duration, contestDone
  FROM contest
  WHERE endPhase1Time > CURRENT_TIMESTAMP() 
  AND type = 'open';
  `;

  const result = await pool.query(sql);
  return result;
}

const contestUser = async (userId) => {
  const sql = `
  SELECT c.id, c.title, c.category, c.type, c.endPhase1Time, c.endPhase2Time
  FROM contest c
  join user_has_contest h ON h.contest_id = c.id
  join user u ON u.id = h.user_id
  WHERE endPhase2Time > CURRENT_TIMESTAMP()
  AND u.id = ${userId};
  `;

  const result = await pool.query(sql);
  return result;
}

const getOrganisers = async () => {
  const sql = `
  SELECT id 
  FROM user 
  WHERE user.role = 'organiser';
  `;

  return await pool.query(sql);
}

const insertUsersToContest = async (userId, contestId) => {
  const sql = `
  INSERT INTO user_has_contest (user_id, contest_id)
  VALUES (?, ?);
  `;

  return await pool.query(sql, [userId, contestId]);
}

const insertJuryToContest = async (userId, contestId) => {
  const sql = `
  INSERT INTO jury (user_id, contest_id)
  VALUES (?, ?);
  `;

  return await pool.query(sql, [userId, contestId]);
}

const getUserEndedContests = async (userId) => {
  const sql = `
  SELECT c.id, c.title, c.category, c.type
  FROM contest c 
  JOIN user_has_contest uhc
  ON c.id = uhc.contest_id
  WHERE uhc.user_id = ?
  AND c.contestDone = 1;
  `;

  return await pool.query(sql, [userId]);
}

export default {
  createContest,
  contestPhase1,
  contestPhase2,
  contestFinished,
  contestOpen,
  contestUser,
  joinTheContest,
  alreadyJoined,
  insertJury,
  getOrganisers,
  insertUsersToContest,
  insertJuryToContest,
  getUserEndedContests
};
