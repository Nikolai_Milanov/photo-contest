const DB_CONFIG = {
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '1234',
    database: 'photocontest'
  }

export const PRIVATE_KEY = 'very_secret_key';

// 60 mins * 60 secs
export const TOKEN_LIFETIME = 60 * 60;

export const DEFAULT_USER_ROLE = 'user';

export const PORT = 3000;

export default DB_CONFIG;