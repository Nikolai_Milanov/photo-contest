import bcrypt from 'bcrypt';

const getUser = userData => {

    return async (column, username) => {
        return await userData.getUser(column, username);
    }
}

const getAllUsers = userData => {
    
    return async () => {
        return await userData.getAllUsers();
    }
}

const getHashedPassword = userData => {

    return async (username, password) => {

        const getHashedPassword = await userData.getHashedPassword('username', username);
        return await bcrypt.compare(password, getHashedPassword.password);
    }
}

const regUser = userData => {

    return async (username, firstName, lastName, password) => {
        const hashedPassword = await bcrypt.hash(password, 10);
        return await userData.regUser(username, firstName, lastName, hashedPassword);
    }
}


export default {
    getUser,
    getHashedPassword,
    regUser,
    getAllUsers
}