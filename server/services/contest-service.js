import serviceErrors from './service-errors.js';
import moment from 'moment';

const createContest = (contestData) => {
  return async ({ title, category, type, endPhase1Time, endPhase2Time }) => {
    const endPhase1 = moment(endPhase1Time);
    const endPhase2 = moment(endPhase2Time);
    const currentTime = moment();

    const phase1Duration = endPhase1.diff(currentTime, 'hours', true);
    const phase2Duration = endPhase2.diff(endPhase1, 'hours', true);

    const formatedPhase1 = moment(endPhase1).format('YYYY-MM-DDTHH:mm:ss');
    const formatedPhase2 = moment(endPhase2).format('YYYY-MM-DDTHH:mm:ss');
    const createdOn = moment(currentTime).format('YYYY-MM-DDTHH:mm:ss');

    const phase1MinLimit = currentTime.add(1, 'days').format('YYYY-MM-DDTHH:mm:ss');
    const phase1MaxLimit = currentTime.add(30, 'days').format('YYYY-MM-DDTHH:mm:ss');

    const phase2MinLimit = endPhase1.add(1, 'hour').format('YYYY-MM-DDTHH:mm:ss');
    const phase2MaxLimit = endPhase1.add(23, 'hours').format('YYYY-MM-DDTHH:mm:ss');

    if (phase1MinLimit > formatedPhase1 || phase1MaxLimit < formatedPhase1) {
      return {
        error: 'Incorrect phase one time limit - Must be from one day to one month!',
        contest: null,
      }
    }

    if (phase2MinLimit > formatedPhase2 || phase2MaxLimit < formatedPhase2) {
      return {
        error: 'Incorrect phase two time limit - Must be from one hour to one day!',
        contest: null,
      }
    }

    if (!title) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null,
      };
    }
    if (!category) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null,
      };
    }
    if (!type) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null,
      };
    }
    if (!endPhase1) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null,
      };
    }
    if (!endPhase2) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null,
      };
    }

    const contest = { title, category, type, formatedPhase1, formatedPhase2, createdOn, phase1Duration, phase2Duration };

    const result = await contestData.createContest(contest);
    return { error: null, contest: { ...contest, id: result.insertId } };
  };
};

const insertJury = (contestData) => {
  return async (contestId) => {
    const organisers = await contestData.getOrganisers();
    organisers.forEach(async organiser => {
      await contestData.insertJury(organiser.id, contestId);
    });
    return { error: null };
  }
}

const joinTheContest = (contestData) => {
  return async ({ userId, contestId }) => {

    const enrollInContest = {
      userId,
      contestId
    }
    return await contestData.joinTheContest(enrollInContest);
  }
}

const alreadyJoined = (contestData) => {
  return async (userId, contestId) => {
    const userJoined = await contestData.alreadyJoined(userId, contestId);
    
    if (userJoined) {
      return {
        error: 'You already have joined in this contest!',
        userJoined: userJoined
      };
    }

    return { error: null, userJoined };
  }
}

const contestPhase1 = (contestData) => {
  return async () => {
    return await contestData.contestPhase1();
  };
};

const contestPhase2 = (contestData) => {
  return async () => {
    return await contestData.contestPhase2();
  };
};

const contestFinished = (contestData) => {
  return async () => {
    return await contestData.contestFinished();
  };
};

const contestOpen = (contestData) => {
  return async () => {
    const contests = await contestData.contestOpen();
    if (contests.length === 0) {
      return {
        error: 'No open contests found!',
        contests: null
      }
    }
    return { error: null, contests: contests };
  };
};

const contestUser = (contestData) => {
  return async (userId) => {
    if (!userId) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        foundContests: null
      };
    } else {
      const foundContests = await contestData.contestUser(userId);

      if (!foundContests) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          foundContests: null
        }
      }

      return { error: null, foundContests: foundContests }
    }
  }
}

const insertUsersToContest = (contestData) => {
  return async (listOfUsers, usersAsJury, contestId) => {
    if (listOfUsers.length !== 0) {
      for (const userId of listOfUsers) {
        await contestData.insertUsersToContest(userId, contestId);
      }
    }
    if (usersAsJury.length !== 0) {
      for (const userId of usersAsJury) {
        await contestData.insertJuryToContest(userId, contestId);
      }
    }
  }
}

const getUserEndedContests = (contestData) => {
  return async (userId) => {
    return await contestData.getUserEndedContests(userId);
  }
}

export default {
  createContest,
  contestPhase1,
  contestPhase2,
  contestFinished,
  contestOpen,
  contestUser,
  joinTheContest,
  alreadyJoined,
  insertJury,
  insertUsersToContest,
  getUserEndedContests
};
