import serviceErrors from './service-errors.js';

/**
 * Uses awarded photo data to get all awarded photos.
 * @param {Object} object with a function that is invoked here.
 * @returns {Function} async function that awaits the response 
 * from the object's function that is invoked.
 */
const getAllAwardedPhotosFromContests = awardedPhotoData => {

    return async () => {
        return await awardedPhotoData.getAllAwardedPhotosFromContests();
    }
}

/**
 * Uses awarded photo data to get all awarded photos.
 * @param {Object} object with a function that is invoked here.
 * @param {Number} number - photo id that is passed 
 * to the invoked function of the object.
 * @returns {Function} async function that awaits the response 
 * from the object's function that is invoked.
 */
const getPhoto = awardedPhotoData => {

    return async (photoId) => {

        const photo = await awardedPhotoData.getPhoto(photoId);
        if (!photo) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                photo: null
            };
        }

        return {error: null, photo};
    }
}


export default {
    getAllAwardedPhotosFromContests,
    getPhoto
}