import moment from 'moment';
import serviceErrors from './service-errors.js';

const getContest = photoData => {

    return async (contestId) => {
        return await photoData.getContest(contestId);
    }
}

const getAllPhotosFromContest = photoData => {

    return async (contestId) => {
        const photos = await photoData.getAllPhotosFromContest(contestId);

        if (photos.length === 0) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                photos: null
            };
        }

        return { error: null, photos };
    }
}

const getPhoto = photoData => {

    return async (photoId, contestId) => {
        return await photoData.getPhoto(photoId, contestId);
    }
}

const getUserPhoto = photoData => {

    return async (userId, contestId) => {
        const userPhoto = await photoData.getUserPhoto(userId, contestId);

        if (userPhoto) {
            return {
                error: serviceErrors.DUPLICATE_RECORD,
                userPhoto: userPhoto
            };
        }

        return { error: null, userPhoto };
    }
}

const uploadPhoto = photoData => {

    return async ({ story, title, image, userId, contestId, endPhase1Time }) => {
        const currentTime = moment().format('YYYY-MM-DDTHH:mm:ss');

        const formattedEndPhase1Time = moment(endPhase1Time).format('YYYY-MM-DDTHH:mm:ss');


        if (currentTime > formattedEndPhase1Time) {
            return { error: 'Cannot upload photo after Phase one of the contest!', photo: null };
        }

        const photo = {
            story, 
            title, 
            image, 
            userId, 
            contestId
        }

        await photoData.uploadPhoto(photo);
        return {error: null, photo };
    }
}

export default {
    getContest,
    getAllPhotosFromContest,
    getPhoto,
    getUserPhoto,
    uploadPhoto
};