import awardedPhotoData from '../data/awardedPhoto-data.js';

const endContest = async () => await awardedPhotoData.endedContests();

const getCompletedContests = async () => await awardedPhotoData.getCompletedContests();

const photosFromEndedContests = async (endedContests) => {
    const arrOfPhotosFromContests = [];
    for (const contest of endedContests) {
        const photosFromContest = await awardedPhotoData.getPhotosToAward(contest.id);
        arrOfPhotosFromContests.push(...photosFromContest);
    }

    return arrOfPhotosFromContests;
}

const photoRates = async (photosFromContest) => {
    const arrOfRatesFromPhotos = [];
    for (const photo of photosFromContest) {
        const photoRates = await awardedPhotoData.getRatesFromPhoto(photo.id, photo.contestId);
        arrOfRatesFromPhotos.push(...photoRates);
    }

    return arrOfRatesFromPhotos;
}

const processTopRates = async (rates) => {
    const contestMap = new Map();

    for (const contest of rates) {
        if (!contestMap.has(contest.contestId)) {
            contestMap.set(contest.contestId, []);
        }

        contestMap.get(contest.contestId).push(contest);
    }
    contestMap.forEach((photoRates) => {
        const sortedContest = photoRates.sort((a, b) => b.score - a.score);
        sortedContest.forEach(async (photo, index) => {
            const contestType = await awardedPhotoData.typeOfContest(photo.contestId);
            let points = 0;
            if (contestType.type === "open") {
                points = 1;
            } else {
                points = 3;
            }
            if (index < 3) {
                if (index === 0) {
                    points += 50;
                    await awardedPhotoData.setUserPoints(points, photo.photoId);
                } else if (index === 1) {
                    points += 35;
                    await awardedPhotoData.setUserPoints(points, photo.photoId);
                } else if (index === 2) {
                    points += 20;
                    await awardedPhotoData.setUserPoints(points, photo.photoId);
                }
                await awardedPhotoData.setAwardedPhotos(photo.photoId);
            }
        });
    });
    console.log('Ended Contests Neutralised!');
}

export {
    endContest,
    getCompletedContests,
    photosFromEndedContests,
    photoRates,
    processTopRates
}