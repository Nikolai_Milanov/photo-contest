import moment from 'moment';
import serviceErrors from './service-errors.js';

const getJuryRate = (rateData) => {
  return async (juryId, photoId, contestId) => {
    const foundId = await rateData.getJuryById(juryId, contestId);

    if (!foundId) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND
      }
    }

    const rate = await rateData.getJuryRate(foundId.id, photoId);

    if (rate) {
      return { error: serviceErrors.OPERATION_NOT_PERMITTED }
    }

    return { error: null, juryId: foundId.id };
  };
};

const getPhase = (rateData) => {
  return async (contestId) => {
    const currentTime = moment().format('YYYY-MM-DDTHH:mm:ss');
    const endPhasesTime = await rateData.getPhase(contestId);

    if (!endPhasesTime) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND
      }
    }

    const formatedEndPhase1Time = moment(endPhasesTime.endPhase1Time).format('YYYY-MM-DDTHH:mm:ss');
    const formatedEndPhase2Time = moment(endPhasesTime.endPhase2Time).format('YYYY-MM-DDTHH:mm:ss');

    if (currentTime < formatedEndPhase1Time && currentTime > formatedEndPhase2Time) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED
      };
    }
    return {
      error: null
    };
  };
};

const ratePhoto = (rateData) => {
  return async (rate) => {
    if (!rate.checkbox) {
      return await rateData.ratePhoto(rate);
    }

    const score = 0;
    const comment = 'Wrong category!';

    const modifiedRate = {
      ...rate,
      score: score,
      comment: comment
    }

    return await rateData.ratePhoto(modifiedRate);
  };
};

const getRateAndComment = (rateData) => {
  return async (contestId, userId, photoId) => {
    // PROVERKA DALI E FINISHED
    return await rateData.getRateAndComment(contestId, userId, photoId);
  }
}

const getPhotoRates = (rateData) => {
  return async (contestId, photoId) => {
    const rates = await rateData.getPhotoRates(contestId, photoId);

    if (rates.length === 0) {
      return {
        error: 'Rates not found!',
        rates: null
      }
    }

    return { error: null, rates: rates };
  }
}

const getPhotoByOtherUsers = (rateData) => {
  return async (contestId) => {
    // PROVERKA DALI E FINISHED
    return await rateData.getPhotoByOtherUsers(contestId);
  }
}

export default {
  getJuryRate,
  ratePhoto,
  getPhase,
  getRateAndComment,
  getPhotoRates,
  getPhotoByOtherUsers,
};
