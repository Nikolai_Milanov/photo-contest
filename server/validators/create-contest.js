export const createContestSchema = {
    title: value => typeof value === 'string' && value.length >= 3 && value.length <= 240,
    category: value => typeof value === 'string' && value.length >= 3 && value.length <= 30,
    type: value => typeof value === 'string' && value.length >= 4 && value.length <= 12,
    endPhase1Time: value => typeof value === 'string',
    endPhase2Time: value => typeof value === 'string',
    listOfUsers: value => Array.isArray(value),
    usersAsJury: value => Array.isArray(value),
  };