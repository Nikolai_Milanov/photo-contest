export const uploadPhotoSchema = {
    title: value => typeof value === 'string' && value.length >= 3 && value.length <= 50,
    story: value => typeof value === 'string' && value.length >= 10 && value.length <= 240,
    image: value => typeof value === 'string'
  };