const validateObject = (schema, object) => Object
  .keys(schema)
  .every(key => schema[key](object[key]));

export default (resourceName, schema) => (req, res, next) => {
  if (validateObject(schema, req.body)) {
    return next();
  }

  res
    .status(400)
    .json({
      error: 'Bad request',
      message: `Invalid ${resourceName} body!`,
    });
};