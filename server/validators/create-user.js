export const createUserSchema = {
    username: value => typeof value === 'string' && value.length >= 3 && value.length <= 240,
    firstName: value => typeof value === 'string' && value.length >= 3 && value.length <= 240,
    lastName: value => typeof value === 'string' && value.length >= 3 && value.length <= 240,
    password: value => typeof value === 'string' && value.length >= 6 && value.length <= 30,
  };