import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import passport from 'passport';
import { PORT } from './config.js';
import userController from './controllers/user-controller.js';
import photoController from './controllers/photo-controller.js';
import contestController from './controllers/contest-controller.js';
import awardedPhotoController from './controllers/awardedPhoto-controller.js';
import rateController from './controllers/rate-controller.js';
import jwtStrategy from './auth/strategy.js';
import {endContest, getCompletedContests, photosFromEndedContests, photoRates, processTopRates } from './services/photoAward.js';


const app = express();
passport.use(jwtStrategy);
app.use(cors(), bodyParser.json());
app.use(passport.initialize());
app.use(userController);
app.use('/contests', photoController);
app.use('/contests', awardedPhotoController);
app.use('/contests', contestController);
app.use('/contests', rateController);

setInterval(async () => {
  await endContest();
  const endedContests = await getCompletedContests();
  const photosFromContests = await photosFromEndedContests(endedContests);
  const rates = await photoRates(photosFromContests);
  await processTopRates(rates);
}, 3600000);

// 86400000 if want to change to 24 hours update...

app.use((err, req, res, next) => {
  console.log(err);
  res.status(500).send({
    message:
      'An unexpected error occurred, our developers are working hard to resolve it.',
  });
});

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
