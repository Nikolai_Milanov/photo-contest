const getToken = () => {
    return localStorage.getItem('token');
}

const setToken = (token) => {
    return localStorage.setItem('token', token);
}

const removeToken = () => {
    return localStorage.removeItem('token');
}

const tokenProvider = {
    getToken,
    setToken,
    removeToken
}

export default tokenProvider;