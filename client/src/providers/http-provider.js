import tokenProvider from "./token-provider";

const get = (url) => {
    const token = tokenProvider.getToken();

    return fetch(url, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
    .then(res => res.json())
};
const post = (url, body) => {
    const token = tokenProvider.getToken();
    
    return fetch(url, {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": 'application/json'
        },
        body: JSON.stringify(body)
    })
    .then(res => res.json())
};

const postUpload = (url, body) => {

    return fetch(url, {
        method: 'POST',
        headers: {
            Authorization: "Client-ID 3a5f178defb2399"
        },
        body: body
    })
    .then(res => res.json())
}

const httpProvider = {
    get,
    post,
    postUpload
};

export default httpProvider;