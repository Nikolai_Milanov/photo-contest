import React from 'react';
import LoadingAnimation from '../media/Bean Eater-1s-200px.svg';

const Loading = () => {

    return (
        <div>
            <img src={LoadingAnimation} alt="Loading" style={{ borderRadius: 100 }} />
        </div>
    )
}

export default Loading;
