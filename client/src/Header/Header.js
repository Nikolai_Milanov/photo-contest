import React from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { NavLink, useHistory } from 'react-router-dom';
import { useAuth } from '../Auth/AuthContext';
import tokenProvider from '../providers/token-provider';
import './Header.css';
import logo from '../media/canstockphoto57717191.jpg'

const Header = () => {
    const { user, setUser } = useAuth();
    const history = useHistory();

    const handleLogOut = () => {
        tokenProvider.removeToken();
        setUser(null);
        history.push('/');
    }

    const handleRootPage = () => {
        history.push('/');
    }

    return (
        <>
            <div className="nav-bar">
                <div className="logo" onClick={handleRootPage}>
                    <img src={logo} alt="logo" />
                    <h3>PHOTO WIZARDS</h3>
                </div>
                {user
                    ?
                    <ButtonGroup className="nav-btns">
                        <Button variant="light" size="lg" id="dashboard-btn" onClick={handleRootPage}>Dashboard</Button>
                        <Button variant="primary" size="lg" id="logout-btn" onClick={handleLogOut}>Logout</Button>
                    </ButtonGroup>
                    :
                    <ButtonGroup className="nav-btns">
                        <NavLink to="/session"><Button variant="primary" size="lg" id="login-btn">Login</Button></NavLink>
                        <NavLink to="register"><Button variant="primary" size="lg" id="register-btn">Register</Button></NavLink>
                    </ButtonGroup>
                }
            </div>
        </>
    )
}

export default Header;