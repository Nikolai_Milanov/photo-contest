import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import httpProvider from '../../providers/http-provider';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import './CreateContestView.css';
import Swal from 'sweetalert2';

const CreateContests = (props) => {
  const [type, setType] = useState(true);
  const [phase1, setPhase1] = useState('');
  const [phase2, setPhase2] = useState('');
  const [users, setUsers] = useState([]);
  const [userList, setUserList] = useState([]);
  const [juryList, setJuryList] = useState([]);
  const { register, getValues } = useForm({ mode: 'onBlur' });
  const history = useHistory();
  useEffect(() => {
    if (!type) {
      httpProvider
        .get(`http://localhost:3000/users`)
        .then((res) => {
          setUsers(res);
        })
        .catch((err) => console.log(err));
    }
  }, [type]);

  const values = getValues();
  const currentTime = moment();
  const phase1MinLimit = currentTime
    .add(1, 'days')
    .format('YYYY-MM-DDTHH:mm:ss');
  const phase1MaxLimit = currentTime
    .add(30, 'days')
    .format('YYYY-MM-DDTHH:mm:ss');
  const formattedPhase1MaxLimit = moment(phase1);
  const phase2MinLimit = formattedPhase1MaxLimit
    .add(1, 'hour')
    .format('YYYY-MM-DDTHH:mm:ss');
  const phase2MaxLimit = formattedPhase1MaxLimit
    .add(23, 'hours')
    .format('YYYY-MM-DDTHH:mm:ss');

  const onSubmit = () => {
    if (type) {
      const valuesCopy = {
        ...values,
        listOfUsers: [],
        usersAsJury: [],
      };
      httpProvider
        .post(`http://localhost:3000/contests`, valuesCopy)
        .then((res) => {
          if (res.contest) {
            Swal.fire({ text: 'Contest Successfully created!' });
            history.push('/dashboard/contests/phase1');
          } else {
            Swal.fire({ text: res.message });
          }
        });
    } else {
      const valuesCopy = {
        ...values,
        listOfUsers: userList,
        usersAsJury: juryList,
      };
      httpProvider
        .post(`http://localhost:3000/contests`, valuesCopy)
        .then((res) => {
          if (res.contest) {
            Swal.fire({ text: 'Contest Successfully created!' });
            history.push('/dashboard/contests/phase1');
          } else {
            Swal.fire({ text: res.message });
          }
        });
    }

  };

  const toggleType = () => {
    setUserList([]);
    setJuryList([]);
    setType(prevState => !prevState);
  }

  const updateUserList = (id, checked) => {
    if (checked) {
      if (juryList.includes(id)) {
        setJuryList(juryList.filter((juryId) => juryId !== id));
      }
      setUserList([...userList, id]);
    } else {
      setUserList(userList.filter((userId) => userId !== id));
    }
  };

  const updateJuryList = (id, checked) => {
    if (checked) {
      if (userList.includes(id)) {
        setUserList(userList.filter((userId) => userId !== id));
      }
      setJuryList([...juryList, id]);
    } else {
      setJuryList(juryList.filter((juryId) => juryId !== id));

    }
  };

  const handleBack = () => {
    history.goBack();
  }

  return (
    <>
      <button onClick={handleBack} className="back-btn">&lt;</button>
      {type ? (
        <div className='form'>
          <h3>Create Contest</h3>
          <label>Title:</label>
          <input className='title' type='text' name='title' ref={register} /> <br />
          <label>Category:</label>
          <input className='category' type='text' name='category' ref={register} /> <br />
          <label>Type:</label>
          <select className='type' name='type' ref={register} onChange={toggleType}>
            <option value='open'>open</option>
            <option value='invitational'>invitational</option>
          </select>{' '}
          <br />
          <label>End phase one date:</label>
          <input className='phase1'
            type='datetime-local'
            name='endPhase1Time'
            onChange={(ev) => setPhase1(ev.target.value)}
            min={phase1MinLimit}
            max={phase1MaxLimit}
            ref={register}
          />
          <br />
          <label>End phase two date:</label>
          <input className='phase2'
            type='datetime-local'
            name='endPhase2Time'
            onChange={(ev) => setPhase2(ev.target.value)}
            min={phase2MinLimit}
            max={phase2MaxLimit}
            ref={register}
          />
          <br />
          <button className='button' onClick={() => onSubmit()}>Create</button>
        </div>
      ) : (
          <div className='form'>
            <h3>Create Invitational Contest</h3>
            <label>Title:</label>
            <input className='title' type='text' name='title' ref={register} /> <br />
            <label>Category:</label>
            <input className='category' type='text' name='category' ref={register} /> <br />
            <label>Type:</label>
            <select className='type' name='type' ref={register} onChange={toggleType}>
              <option value='open'>open</option>
              <option value='invitational'>invitational</option>
            </select>
            <br />
            <label>End phase one date:</label>
            <input
              className='phase1'
              type='datetime-local'
              name='endPhase1Time'
              onChange={(ev) => setPhase1(ev.target.value)}
              min={phase1MinLimit}
              max={phase1MaxLimit}
              ref={register}
            />
            <br />
            <label>End phase two date:</label>
            <input
              className='phase2'
              type='datetime-local'
              name='endPhase2Time'
              onChange={(ev) => setPhase2(ev.target.value)}
              min={phase2MinLimit}
              max={phase2MaxLimit}
              ref={register}
            />
            <br />
            <div style={{ paddingTop: '10px' }}>
              {users.map((user) => {
                return (
                  <div key={user.id}>
                    <span>
                      <span>Username: <span style={{ fontWeight: 'bold' }}>{user.username}</span> - </span>
                      <label style={{ paddingRight: '5px' }}>Invite</label>
                      <input
                        type='checkbox'
                        onClick={(e) => updateUserList(user.id, e.target.checked)}
                      ></input>
                      {user.points >= 151 ? (
                        <>
                          <label style={{ paddingLeft: '5px', marginLeft: '5px', borderLeft: "2px solid black" }}>Invite as jury</label>{' '}
                          <input
                            type='checkbox'
                            onClick={(e) =>
                              updateJuryList(user.id, e.target.checked)
                            }
                          ></input>{' '}
                        </>
                      ) : null}
                    </span>
                  </div>
                );
              })}
            </div>
            <button className='button' onClick={() => onSubmit()}>Create</button>
          </div>
        )}
    </>
  ); // SHOW USERS
};

export default CreateContests;
