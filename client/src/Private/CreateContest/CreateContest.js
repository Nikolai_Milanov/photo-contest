import React from 'react'
import CreateContests from './CreateContestView';

const CreateContest = () => {

    return (
        <div>
            <h1 className="page-title">Create Contest</h1>
            <CreateContests/>
        </div>
    )
}

export default CreateContest;