import React, { useEffect, useState } from 'react';
import httpProvider from '../../../providers/http-provider';
import PhaseFinishedView from './phaseFinishedView';
import Loading from '../../../Loading/Loading';
import { useHistory } from 'react-router-dom';
import noContestsImg from '../../../media/surprised pikachu.jpg';

const ContestsFinished = () => {
  const [contestsFinished, setContestsFinished] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  const url = 'http://localhost:3000/contests/finished';

  const handleBack = () => {
    history.goBack();
  }

  useEffect(() => {
    httpProvider
      .get(url)
      .then((result) => {
        setTimeout(() => {
        if(!result.message) {
          setContestsFinished(result);
        }
        setLoading(false);
      }, 500)
      })
      .catch((err) => console.log(err));
  }, [url]);

  if (loading) {
    return <Loading />;
}

  return (
    <div>
      <button onClick={handleBack} className="back-btn">&lt;</button>
      <h1 className="page-title">Finished Contests</h1>
      {contestsFinished.length ? (
        <div>
          {contestsFinished.map((contest) => {
            return (
              <PhaseFinishedView
                key={contest.id}
                id={contest.id}
                title={contest.title}
                category={contest.category}
                type={contest.type}
              />
            );
          })}
        </div>
      ) : (
        <div className="no-contests-container">
          <h3>No contests found!</h3>
          <img src={noContestsImg} alt="not-found-img"/>
        </div>
      )}
    </div>
  );
};

export default ContestsFinished;
