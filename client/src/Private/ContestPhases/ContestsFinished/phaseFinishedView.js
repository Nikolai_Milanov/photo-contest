import React from 'react';
import './phaseFinishedView.css';
import Card from 'react-bootstrap/Card';

const PhaseFinishedView = (props) => {
  const { id, title, category, type } = props;

  return (
    <Card style={{ width: '25rem' }}>
      <Card.Body>
        <Card.Title>Contest Number: {id}</Card.Title>
        <Card.Title>Title: {title}</Card.Title>
        <Card.Title>Category: {category}</Card.Title>
        <Card.Title>Type: {type}</Card.Title>
      </Card.Body>
    </Card>
  );
};

export default PhaseFinishedView;
