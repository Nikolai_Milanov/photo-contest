import React from 'react';

const PhotoView = (props) => {
  const { photo } = props;

  return (
    <div>
      <br />
      <h3>Title: {photo.title}</h3>
      <img alt='snimka' src={photo.image} style={{ maxWidth: 450 }} />
      <h3>Story: {photo.story}</h3>
      <hr style={{ border: '1px solid black' }} />
    </div>
  );
};

export default PhotoView;
