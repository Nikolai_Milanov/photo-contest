import React, { useEffect, useState } from 'react';
import httpProvider from '../../../providers/http-provider';
import PhaseOneView from './phase1View';
import Loading from '../../../Loading/Loading';
import { useHistory } from 'react-router-dom';
import noContestsImg from '../../../media/a765w3L2_700w_0.jpg'

// DA SE POKAZVA OSTAVASHTOTO VREME

const ContestsPhase1 = () => {
  const [contestsPhase1, setContestsPhase1] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  const url = 'http://localhost:3000/contests/phase1';

  const handleBack = () => {
    history.goBack();
  }

  useEffect(() => {
    httpProvider
      .get(url)
      .then((result) => {
        setTimeout(() => {
          if (!result.error) {
            setContestsPhase1(result);
          }
          setLoading(false);
        }, 500)
        })
        .catch((error) => console.log(error));
  }, [url]);

  if (loading) {
    return <Loading />;
  }

  return (
    <div>
      <button onClick={handleBack} className="back-btn">&lt;</button>
      <h1 className="page-title">Contests Phase One</h1>
      {contestsPhase1.length ? (
        <div>
          {contestsPhase1.map((contest) => {
            return (
              <PhaseOneView
                key={contest.id}
                id={contest.id}
                title={contest.title}
                category={contest.category}
                type={contest.type}
                contest={contest}
              />
            );
          })}
        </div>
      ) : (
        <div className="no-contests-container">
          <h3>No contests found!</h3>
          <img src={noContestsImg} alt="not-found-img" />
        </div>
      )}
    </div>
  );
};

export default ContestsPhase1;
