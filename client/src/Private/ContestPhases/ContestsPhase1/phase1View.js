import React, { useState } from 'react';
import httpProvider from '../../../providers/http-provider';
import Card from 'react-bootstrap/Card';
import PhotoView from './PhotoView';
import Countdown from '../../../Countdown/Countdown';
import moment from 'moment';
import '../../../Countdown/Countdown.css';

const PhaseOneView = (props) => {
  const { id, title, category, type, contest } = props;
  const [photos, setPhotos] = useState([]);
  const [showPhotos, setShowPhotos] = useState(false);
  const currentTime = moment();

  const getPhotos = (contestId) => {
    httpProvider
      .get(`http://localhost:3000/contests/${contestId}/photos`)
      .then((result) => {
        if (!result.message) {
          setPhotos(result);
        }
      })
      .catch((err) => alert(err));
    setShowPhotos((prevState) => !prevState);
  };

  return (
    <Card style={{ width: '25rem' }}>
      <Card.Body>
        {contest.endPhase1Time > currentTime.format('YYYY-MM-DDTHH:mm:ss') ? (
          <Countdown phase1End={contest.endPhase1Time} />
        ) : (
            <Countdown phase2End={contest.endPhase2Time} />
          )}
        <Card.Title>Contest Number: {id}</Card.Title>
        <Card.Title>Title: {title}</Card.Title>
        <Card.Title>Category: {category}</Card.Title>
        <Card.Title>Type: {type}</Card.Title>
        {showPhotos ? (
          <div>
            <button onClick={() => setShowPhotos(false)} className="toggle-button">Hide Photos</button>
            {photos.length === 0 ?
              <Card.Title style={{ paddingTop: 10 }}>There are no submitted photos!</Card.Title>
              :
              <>
                {photos.map((photo) => <PhotoView key={photo.id} photo={photo} contest={contest} />)}
              </>
            }
          </div>
        ) : (
            <>
              <button onClick={() => getPhotos(contest.id)}className="toggle-button">Show Photos</button>
            </>
          )}
      </Card.Body>
    </Card>
  );
};

export default PhaseOneView;
