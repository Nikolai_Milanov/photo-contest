import React, { useState, useEffect } from 'react';
import httpProvider from '../../../providers/http-provider';
import PhaseTwoView from './phase2View';
import Loading from '../../../Loading/Loading';
import { useHistory } from 'react-router-dom';
import noContestsImg from '../../../media/azalDDZp_700w_0.jpg';

const ContestsPhase2 = () => {
  const [contestsPhase2, setContestsPhase2] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  
  const url = 'http://localhost:3000/contests/phase2';

  const handleBack = () => {
    history.goBack();
  }
  
  useEffect(() => {
    httpProvider
      .get(url)
      .then((result) => {
        setTimeout(() => {
          if (!result.message) {
            setContestsPhase2(result);
          }
          setLoading(false);
        }, 500);
      })
      .catch((err) => console.log(err));
  }, [url]);

  if (loading) {
    return <Loading />;
  }

  return (
    <div>
      <button onClick={handleBack} className="back-btn">&lt;</button>
      <h1 className="page-title">Contests Phase Two</h1>
      {contestsPhase2.length ? (
        <div>
          {contestsPhase2.map((contest) => {
            return (
              <PhaseTwoView
                key={contest.id}
                id={contest.id}
                title={contest.title}
                category={contest.category}
                type={contest.type}
                contest={contest}
              />
            );
          })}
        </div>
      ) : (
        <div className="no-contests-container">
          <h3>No contests found!</h3>
          <img src={noContestsImg} alt="not-found-img"/>
        </div>
      )}
    </div>
  );
};

export default ContestsPhase2;
