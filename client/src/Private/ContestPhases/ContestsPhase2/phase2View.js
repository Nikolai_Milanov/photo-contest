import React from 'react';
import Card from 'react-bootstrap/Card';
import Countdown from '../../../Countdown/Countdown';
import moment from 'moment';

const PhaseTwoView = (props) => {
  const { id, title, category, type, contest } = props;
  const currentTime = moment();

  return (
    <Card style={{ width: '25rem' }}>
      <Card.Body>
        {contest.endPhase1Time > currentTime.format('YYYY-MM-DDTHH:mm:ss') ? (
          <Countdown phase1End={contest.endPhase1Time} />
        ) : (
          <Countdown phase2End={contest.endPhase2Time} />
        )}
        <Card.Title>ID: {id}</Card.Title>
        <Card.Title>Title: {title}</Card.Title>
        <Card.Title>Category: {category}</Card.Title>
        <Card.Title>Type: {type}</Card.Title>
      </Card.Body>
    </Card>
  );
};

export default PhaseTwoView;
