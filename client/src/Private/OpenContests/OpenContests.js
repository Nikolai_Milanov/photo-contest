import React, { useState, useEffect } from 'react';
import httpProvider from '../../providers/http-provider';
import { useAuth } from '../../Auth/AuthContext';
import Loading from '../../Loading/Loading';
import Countdown from '../../Countdown/Countdown';
import Card from 'react-bootstrap/Card';
import { useHistory } from 'react-router-dom';
import NoOpenContestsImg from '../../media/a5jorQXn_700w_0.jpg';
import Swal from 'sweetalert2';

const OpenContests = () => {
  const { user } = useAuth();
  const [contests, setContests] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  const handleBack = () => {
    history.goBack();
  }
  useEffect(() => {
    let mounted = true;

    httpProvider
      .get(`http://localhost:3000/contests/open`)
      .then((res) => {
        if (mounted) {
          setTimeout(() => {
            if (!res.message) {
              setContests(res); //
            }
            setLoading(false);
          }, 500);
        }
      })
      .catch((err) => console.log(err));

    return () => {
      mounted = false;
    };
  }, []);

  const enrollContest = (contestId) => {
    httpProvider
      .post(
        `http://localhost:3000/contests/join/${contestId}/users/${user.sub}`
      )
      .then((res) => {
        if (res.userJoined) {
          Swal.fire({ text: res.message });
          setContests((prevContests) =>
            prevContests.filter(
              (contest) => contest.id !== res.userJoined.contest_id
            )
          );
        } else {
          Swal.fire({ text: res.message });
        }
      })
      .catch((err) => console.log(err));
  };

  if (loading) {
    return <Loading />;
  }

  return (
    <div>
      <button onClick={handleBack} className="back-btn">&lt;</button>
      <h1 className="page-title">OPEN CONTESTS</h1>
      {contests.length !== 0 ? (
        contests.map((contest) => {
          return (
            <div key={contest.id}>
              <Card style={{ width: '25rem' }}>
                <Card.Body>
                  <Countdown phase1End={contest.endPhase1Time} />
                  <Card.Title>Contest Number: {contest.id}</Card.Title>
                  <Card.Title>Title: {contest.title}</Card.Title>
                  <Card.Title>Category: {contest.category}</Card.Title>
                  <Card.Title>Type: {contest.type}</Card.Title>
                  <button className="submit-button" onClick={() => enrollContest(contest.id)}>
                    Enroll and give it a SHOT!
                  </button>
                </Card.Body>
              </Card>
            </div>
          );
        })
      ) : (
          <div className="no-contests-container">
            <h3>Currently there aren't any open contests to enroll.</h3>
            <img src={NoOpenContestsImg} alt="not-found-img" />
          </div>
        )}
    </div>
  );
};

export default OpenContests;
