import React, { useState, useRef, useEffect } from 'react';
import httpProvider from '../../providers/http-provider';
import { useAuth } from '../../Auth/AuthContext';
import moment from 'moment';
import '../UserDashboard/UserDashboard.css';
import Swal from 'sweetalert2';
import { Card } from 'react-bootstrap';

const SingleContest = (props) => {
    const [file, setFile] = useState(null);
    const [warning, setWarning] = useState(false);
    const [submittedPhoto, setSubmittedPhoto] = useState(false);
    const [viewPhoto, setViewPhoto] = useState(false);
    const [userPhoto, setUserPhoto] = useState({});
    const [loading, setLoading] = useState(true);
    const currentTime = moment();
    const phase1End = props.contest.endPhase1Time;
    const isAfterPhase1 = currentTime.isAfter(phase1End);
    const { contest } = props;
    const title = useRef();
    const story = useRef();
    const { user } = useAuth();

    useEffect(() => {
        httpProvider.get(`http://localhost:3000/contests/${contest.id}/photo/user/${user.sub}`)
            .then(res => {
                if (res.userPhoto) {
                    setUserPhoto(res.userPhoto);
                    setSubmittedPhoto(true);
                }
                setLoading(false);
            })
            .catch(err => console.log(err));
    }, [contest.id, user.sub])

    useEffect(() => {
        if (viewPhoto) {
            httpProvider.get(`http://localhost:3000/contests/${contest.id}/photo/user/${user.sub}`)
                .then(res => {
                    if (res.userPhoto) {
                        setUserPhoto(res.userPhoto);
                    }
                })
                .catch(err => console.log(err));
        }
    }, [contest.id, user.sub, viewPhoto])

    const fileToUpload = (ev) => {
        if (ev.target.files.length) {
            setWarning(true);
            Swal.fire({
                text: `
                WARNING: If you upload this photo, there will be no turning back!
                You are able to upload only one photo!`
            });
        }
        setFile(ev.target.files[0]);
    }

    const upload = (contestId) => {
        if (title.current.value) {
            if (title.current.value.length < 3 || title.current.value.length > 50) {
                return Swal.fire({ text: 'The Title must be between 3-50 letters!' });
            }
        } else {
            return Swal.fire({ text: 'You have to give a title for this photo!' });
        }
        if (story.current.value) {
            if (story.current.value.length < 10 || story.current.value.length > 240) {
                return Swal.fire({ text: 'The Story must be between 10-240 letters!' });
            }
        } else {
            return Swal.fire({ text: 'You have to give a story for this photo!' });
        }
        if (!file) {
            return Swal.fire({ text: 'You have to pick a photo!' });
        }
        httpProvider.postUpload('https://api.imgur.com/3/image', file)
            .then(res => uploadToBase(res.data.link, contestId))
            .catch(err => console.log(err));
    }

    const uploadToBase = (uploadFileToBase, contestId) => {
        const requestBody = {
            "story": story.current.value,
            "title": title.current.value,
            "image": uploadFileToBase,
            "userId": user.sub
        };

        httpProvider.post(`http://localhost:3000/contests/${contestId}/photos`, requestBody)
            .then(res => {
                if (res.uploadedPhoto) {
                    Swal.fire({ text: res.message });
                    setSubmittedPhoto(true);
                } else {
                    Swal.fire({ text: res.message });
                }
            })
            .catch(err => console.log(err));
    }

    const toggleViewPhoto = () => setViewPhoto(prevState => !prevState);

    if (loading) {
        return null
    }

    return (
        <div>
            {isAfterPhase1
                ?
                <>
                    {viewPhoto
                        ?
                        <>
                            {userPhoto.id
                                ?
                                <div>
                                    <Card.Title>Contest Number: {contest.id}</Card.Title>
                                    <Card.Title>Contest Title: {contest.title}</Card.Title>
                                    <Card.Title>Category: {contest.category}</Card.Title>
                                    <Card.Title>Type: {contest.type}</Card.Title>
                                    <hr style={{ border: '1px solid black' }} />
                                    <h3>The contest phase 1 finished.</h3>
                                    <hr style={{ border: '1px solid black' }} />
                                    <div>
                                        <button onClick={toggleViewPhoto} className="toggle-button">Hide submitted Photo</button>
                                        <div className="uploaded-img">
                                            <img src={userPhoto.image} className="photo" alt="contest_photo" />
                                        </div>
                                        <div className="uploaded-img-info">
                                            <h3>Title: {userPhoto.title}</h3>
                                            <h3>Story: {userPhoto.story}</h3>
                                        </div>
                                    </div>
                                </div>
                                :
                                <div>
                                    <Card.Title>Contest Number: {contest.id}</Card.Title>
                                    <Card.Title>Contest Title: {contest.title}</Card.Title>
                                    <Card.Title>Category: {contest.category}</Card.Title>
                                    <Card.Title>Type: {contest.type}</Card.Title>
                                    <hr style={{ border: '1px solid black' }} />
                                    <h3>The contest phase 1 finished.</h3>
                                    <div>
                                        <button onClick={toggleViewPhoto} className="toggle-button">Hide info</button>
                                        <hr style={{ border: '1px solid black' }} />
                                        <h3>You haven't uploaded photo <br /> for this contest.</h3>
                                    </div>
                                </div>
                            }
                        </>
                        :
                        <div onClick={toggleViewPhoto} style={{ cursor: "pointer" }}>
                            <Card.Title>Contest Number: {contest.id}</Card.Title>
                            <Card.Title>Contest Title: {contest.title}</Card.Title>
                            <Card.Title>Category: {contest.category}</Card.Title>
                            <Card.Title>Type: {contest.type}</Card.Title>
                            <hr style={{ border: '1px solid black' }} />
                            <h3 style={{ padding: '10px' }}>The contest phase 1 finished.</h3>
                        </div>
                    }
                </>
                :
                <>
                    <div>
                        <Card.Title>Contest Number: {contest.id}</Card.Title>
                        <Card.Title>Contest Title: {contest.title}</Card.Title>
                        <Card.Title>Category: {contest.category}</Card.Title>
                        <Card.Title>Type: {contest.type}</Card.Title>
                        {submittedPhoto
                            ?
                            <>
                                {viewPhoto
                                    ?
                                    <div>
                                        <button onClick={toggleViewPhoto} className="toggle-button">Hide submitted Photo</button>
                                        <div className="uploaded-img-info">
                                            <h3>Title: {userPhoto.title}</h3>
                                            <div className="uploaded-img">
                                                <img src={userPhoto.image} className="photo" alt="contest_photo" />
                                            </div>
                                            <h3>Story: {userPhoto.story}</h3>
                                        </div>
                                    </div>
                                    :
                                    <div className="uploaded-img-info">
                                        <hr style={{ border: '1px solid black' }} />
                                        <h3>You already uploaded a photo <br /> for this contest.</h3>
                                        <button onClick={toggleViewPhoto} className="toggle-button">View submitted Photo</button>
                                    </div>
                                }
                            </>
                            :
                            <>
                                <div>
                                    <p>
                                        <span className="photo-input" id="photo-title">Title: </span><input type="text" ref={title} />
                                    </p>
                                    <p>
                                        <span className="photo-input">Story: </span><input type="text" ref={story} />
                                    </p>
                                </div>
                                {warning
                                    ?
                                    <div>
                                        <button onClick={() => { setFile(null); setWarning(false) }} className="clear-button">Clear selected photo</button>
                                    </div>
                                    :
                                    <div>
                                        <input type="file" style={{ display: 'none' }} id={contest.id} accept="image/png, image/jpeg" onChange={fileToUpload} />
                                        <label htmlFor={contest.id} className="pick-photo-button">Pick a Photo</label>
                                    </div>
                                }
                                <button onClick={() => upload(contest.id)} className="submit-button">Submit Photo</button>
                            </>
                        }
                    </div>
                </>
            }
        </div>
    )
}

export default SingleContest;
