import React from 'react';
import { Card } from 'react-bootstrap';

const OrganiserDashboard = (props) => {

    const redirectCreate = () => {
        props.history.push("/dashboard/contests/create");
    }

    const redirectPhase1 = () => {
        props.history.push("/dashboard/contests/phase1");
    }

    const redirectPhase2 = () => {
        props.history.push("/dashboard/contests/phase2");
    }

    const redirectFinished = () => {
        props.history.push("/dashboard/contests/finished");
    }

    const redirectUsers = () => {
        props.history.push("/dashboard/users");
    }

    const redirectRate = () => {
        props.history.push("/dashboard/contests/ratecontest");
    }

    return (
        <>
            <h1 className="page-title">ORGANISER DASHBOARD</h1>
            <Card style={{ width: '15rem' }} onClick={redirectCreate}>
                <Card.Body>
                    <Card.Title>Create Contest</Card.Title>
                </Card.Body>
            </Card>
            <Card style={{ width: '15rem' }} onClick={redirectPhase1}>
                <Card.Body>
                    <Card.Title>Phase 1 Contests</Card.Title>
                </Card.Body>
            </Card>
            <Card style={{ width: '15rem' }} onClick={redirectPhase2}>
                <Card.Body>
                    <Card.Title>Phase 2 Contests</Card.Title>
                </Card.Body>
            </Card>
            <Card style={{ width: '15rem' }} onClick={redirectFinished}>
                <Card.Body>
                    <Card.Title>Finished Contests</Card.Title>
                </Card.Body>
            </Card>
            <Card style={{ width: '15rem' }} onClick={redirectUsers}>
                <Card.Body>
                    <Card.Title>Photo Junkies</Card.Title>
                </Card.Body>
            </Card>
            <Card style={{ width: '15rem' }} onClick={redirectRate}>
                <Card.Body>
                    <Card.Title>Rate photos</Card.Title>
                </Card.Body>
            </Card>
        </>
    )
}

export default OrganiserDashboard;