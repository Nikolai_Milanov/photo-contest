import React, { useEffect, useState } from 'react';
import httpProvider from '../../providers/http-provider';
import UsersView from './photoJunkiesView';
import Loading from '../../Loading/Loading';
import { useHistory } from 'react-router-dom';
import NoUsersImg from '../../media/sad looking cat.jpg';

const PhotoJunkies = () => {
  const [photoJunkies, setPhotoJunkies] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  const url = 'http://localhost:3000/users';

  const handleBack = () => {
    history.goBack();
  }

  useEffect(() => {
    httpProvider.get(url).then((result) => {
      setTimeout(() => {
        if (!result.message) {
          setPhotoJunkies(result);
        }
        setLoading(false);
      }, 500);
    });
  }, [url]);

  if (loading) {
    return <Loading />;
  }

  return (
    <div>
      <button onClick={handleBack} className="back-btn">&lt;</button>
      <h1 className="page-title">Photo Junkies</h1>
      <>
        {photoJunkies.length !== 0
          ?
          <>
            {photoJunkies.map((user) => {
              return (
                <UsersView
                  key={user.id}
                  id={user.id}
                  username={user.username}
                  firstName={user.firstName}
                  lastName={user.lastName}
                  points={user.points}
                />
              );
            })}
          </>
          :
          <div className="no-contests-container">
            <h3>No users found!</h3>
            <img src={NoUsersImg} alt="not-found-img"/>
          </div>
        }
      </>
    </div>
  );
};

export default PhotoJunkies;
