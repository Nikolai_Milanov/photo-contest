import React from 'react';
import Card from 'react-bootstrap/Card';

const UsersView = (props) => {
    const { id, username, firstName, lastName, points } = props;

    return (
        <Card style={{ width: '25rem' }}>
        <Card.Body>
          <Card.Title>ID: {id}</Card.Title>
          <Card.Title>Username: {username}</Card.Title>
          <Card.Title>First name: {firstName}</Card.Title>
          <Card.Title>Last name: {lastName}</Card.Title>
          <Card.Title>Points: {points}</Card.Title>
        </Card.Body>
      </Card>
    )
}

export default UsersView;