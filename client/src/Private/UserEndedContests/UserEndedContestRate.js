import React, { useEffect, useState } from 'react';
import { useAuth } from '../../Auth/AuthContext';
import httpProvider from '../../providers/http-provider';
import Loading from '../../Loading/Loading';
import './UserEndedContestRate.css';

const UserEndedContestRate = (props) => {
    const { contest } = props;
    const { user } = useAuth();
    const [rates, setRates] = useState([]);
    const [userPhoto, setUserPhoto] = useState({});
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        httpProvider.get(`http://localhost:3000/contests/${contest.id}/photo/user/${user.sub}`)
            .then(res => {
                if (res.userPhoto) {
                    setUserPhoto(res.userPhoto);
                }
            })
            .catch(err => console.log(err));

    }, [contest.id, user.sub])

    useEffect(() => {
        httpProvider.get(`http://localhost:3000/contests/${contest.id}/rates/user/${user.sub}/photo/${userPhoto.id}`)
            .then(res => {
                if (res.length === 0) {
                    return null;
                } else if (res.length !== 0) {
                    setRates(res);
                }
            })
            .catch(err => console.log(err));
        setTimeout(() => {
            setLoading(false);
        }, 500);

    }, [contest.id, user.sub, userPhoto.id])


    if (loading) {
        return <Loading />
    }

    return (
        <div>
            {userPhoto.id
                ?
                <div>
                    <div>
                        <h3>Title: {userPhoto.title}</h3>
                        <img src={userPhoto.image} alt="contest_photo" className="user-photo" />
                        <h3>Story: {userPhoto.story}</h3>
                        <hr style={{ border: '1px solid black' }} />
                    </div>
                    {rates.length !== 0
                        ?
                        <div>
                            {rates.map(rate => {
                                return (
                                    <div key={rate.id}>
                                        <h3>Score: {rate.score}</h3>
                                        <h3>Comment: {rate.comment}</h3>
                                        <hr style={{ border: '1px solid black' }} />
                                    </div>
                                )
                            })}
                        </div>
                        :
                        <div>
                            <h3>You haven't been rated!</h3>
                        </div>
                    }
                </div>
                :
                <div>
                    <h3>You haven't uploaded photo for this contest.</h3>
                </div>
            }
        </div>
    )
}

export default UserEndedContestRate;
