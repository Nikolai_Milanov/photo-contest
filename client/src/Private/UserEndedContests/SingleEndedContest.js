import React, { useState } from 'react';
import UserEndedContestRate from './UserEndedContestRate';
import Card from 'react-bootstrap/Card';

const SingleEndedContest = (props) => {
  const [contest] = useState(props.endedContest);
  const [userRate, setUserRate] = useState(false);

  const toggleUserRate = () => setUserRate((prevState) => !prevState);

  return (
    <div>
      {userRate ? (
        <div>
          <Card style={{ width: '25rem' }}>
            <Card.Body>
              <button onClick={toggleUserRate} className="toggle-button">Hide Rates</button>
              <Card.Title>Contest Number: {contest.id}</Card.Title>
              <Card.Title>Title: {contest.title}</Card.Title>
              <Card.Title>Category: {contest.category}</Card.Title>
              <Card.Title>Type: {contest.type}</Card.Title>
              <UserEndedContestRate contest={contest} />
            </Card.Body>
          </Card>
        </div>
      ) : (
        <div onClick={toggleUserRate} style={{ cursor: 'pointer' }}>
            <Card style={{ width: '25rem' }}>
            <Card.Body>
              <Card.Title>Contest Number: {contest.id}</Card.Title>
              <Card.Title>Title: {contest.title}</Card.Title>
              <Card.Title>Category: {contest.category}</Card.Title>
              <Card.Title>Type: {contest.type}</Card.Title>
            </Card.Body>
          </Card>
        </div>
      )}
    </div>
  );
};

export default SingleEndedContest;
