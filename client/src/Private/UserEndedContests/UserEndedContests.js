import React, { useEffect, useState } from 'react';
import { useAuth } from '../../Auth/AuthContext';
import httpProvider from '../../providers/http-provider';
import Loading from '../../Loading/Loading';
import SingleEndedContest from './SingleEndedContest';
import { useHistory } from 'react-router-dom';
import NoEndedContestsImg from '../../media/pablo escobar waiting meme.jpg';

const UserEndedContests = () => {
    const { user } = useAuth();
    const [contests, setContests] = useState([]);
    const [loading, setLoading] = useState(true);
    const history = useHistory();

    const handleBack = () => {
        history.goBack();
    }

    useEffect(() => {
        let mounted = true;
        httpProvider.get(`http://localhost:3000/contests/users/${user.sub}`)
        .then(res => {
            if (mounted) {
                setTimeout(() => {
                    if (res.length !== 0) {
                        setContests(res);
                    }
                    setLoading(false);
                }, 500);
            }
        })
        .catch(err => console.log(err));

        return () => {
            mounted = false;
        }
    }, [user.sub])

    if (loading) {
        return <Loading />
    }

    return (
        <div>
            <button onClick={handleBack} className="back-btn">&lt;</button>
            <h1 className="page-title">ENDED CONTESTS {user.username} PARTICIPATED IN:</h1>
            {contests.length !== 0
                ?
                contests.map(contest => {
                    return (
                        <div key={contest.id}>
                            <SingleEndedContest endedContest={contest} />
                        </div>
                    );
                })
                :
                <div className="no-contests-container">
                    <h3>You haven't participated in any finished contest.</h3>
                    <img src={NoEndedContestsImg} alt="not-found-img"/>
                </div>
            }
        </div>
    )
}

export default UserEndedContests;