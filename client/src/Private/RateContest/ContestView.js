import React, { useState } from 'react';
import Card from 'react-bootstrap/Card';
import httpProvider from '../../providers/http-provider';
import PhotoView from './PhotoView';
import './ContestView.css';
import Countdown from '../../Countdown/Countdown';
import moment from 'moment';

const ContestView = (props) => {
  const { contest } = props;
  const [photos, setPhotos] = useState([]);
  const [showPhotos, setShowPhotos] = useState(false);
  const currentTime = moment();

  const getPhotos = (contestId) => {
    httpProvider
      .get(`http://localhost:3000/contests/${contestId}/photos`)
      .then((result) => {
        if (!result.message) {
          setPhotos(result);
        }
      })
      .catch((err) => alert(err));
    setShowPhotos((prevState) => !prevState);
  };

  return (
    <Card key={contest.id} style={{ width: '25rem' }}>
      <Card.Body>
      {contest.endPhase1Time > currentTime.format('YYYY-MM-DDTHH:mm:ss') ? (
          <Countdown phase1End={contest.endPhase1Time} />
        ) : (
          <Countdown phase2End={contest.endPhase2Time} />
        )}
        <Card.Title>Contest Number: {contest.id}</Card.Title>
        <Card.Title>Title: {contest.title}</Card.Title>
        <Card.Title>Category: {contest.category}</Card.Title>
        <Card.Title>Type: {contest.type}</Card.Title>
        {showPhotos ? (
          <Card.Body>
            <button onClick={() => setShowPhotos(false)} style={{borderRadius: '10px'}}>Hide Photos</button>
            {photos.length === 0 ?
              <Card.Title>There are no submitted photos!</Card.Title>
              :
              <>
                {photos.map((photo) => <PhotoView key={photo.id} photo={photo} contest={contest} />)}
              </>
            }
          </Card.Body>
        ) : (
            <>
              <button onClick={() => getPhotos(contest.id)} style={{borderRadius: '10px'}}>Show Photos</button>
            </>
          )}
      </Card.Body>
    </Card>
  );
};

export default ContestView;
