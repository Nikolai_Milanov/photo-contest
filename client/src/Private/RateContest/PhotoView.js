import React, { useState, useRef, useEffect } from 'react';
import { useAuth } from '../../Auth/AuthContext';
import httpProvider from '../../providers/http-provider';
import Swal from 'sweetalert2';

const PhotoView = (props) => {
  const { photo, contest } = props;
  const { user } = useAuth();
  const [checkbox, setCheckbox] = useState(false);
  const [alreadyRated, setAlreadyRated] = useState(false);
  const score = useRef(null);
  const comment = useRef(null);

  useEffect(() => {
    httpProvider.get(`http://localhost:3000/contests/${contest.id}/rates/${user.sub}/photo/${photo.id}`)
      .then(res => {
        if (res.length !== 0) {
          setAlreadyRated(true);
        }
      })
      .catch(err => console.log(err));
  }, [contest.id, user.sub, photo.id])

  const ratePhoto = () => {
    if (checkbox) {
      const rate = {
        checkbox: true,
        juryId: user.sub
      }
      httpProvider.post(`http://localhost:3000/contests/${contest.id}/rates/photo/${photo.id}`, rate)
        .then((res) => {
          if (res) {
            Swal.fire({ text: 'Photo Successfully rated!' });
          } else {
            Swal.fire({ text: res.message });
          }
        })
    } else {
      if (!comment.current.value) {
        return Swal.fire({ text: 'You must give a comment in order to rate the photo!' })
      }
      const rate = {
        score: score.current.value,
        comment: comment.current.value,
        checkbox: false,
        juryId: user.sub
      }
      httpProvider.post(`http://localhost:3000/contests/${contest.id}/rates/photo/${photo.id}`, rate)
        .then((res) => {
          if (res.rate) {
            Swal.fire({ text: 'Photo Successfully rated!' });
            setAlreadyRated(true);
          } else {
            Swal.fire({ text: res.message });
          }
        })
    }
  }

  return (
    <div key={photo.id}>
      <br />
      <h3>Title: {photo.title}</h3>
      <img alt='snimka' src={photo.image} style={{ maxWidth: 450 }} />
      <h3>Story: {photo.story}</h3>
      <>
        {
          alreadyRated ?
            <div>
              <hr style={{ border: '1px solid black' }} />
              <h3>This photo is already rated!</h3>
            </div>
            :
            <div style={{ borderRadius: '10px', background: 'whitesmoke', marginBottom: '10px' }}>
              <label style={{ marginRight: '10px', fontSize: '20px' }}>Wrong Catergory</label><input name='checkbox' type='checkbox' onClick={() => setCheckbox(prevState => !prevState)}></input>
              {
                checkbox ?
                  <button onClick={ratePhoto} className="submit-button" style={{ marginLeft: '10px' }}>Rate!</button>
                  :
                  <div>
                    <label>Rate: </label>
                    <select name='rate' ref={score}>
                      <option value='1'>1</option>
                      <option value='2'>2</option>
                      <option value='3'>3</option>
                      <option value='4'>4</option>
                      <option value='5'>5</option>
                      <option value='6'>6</option>
                      <option value='7'>7</option>
                      <option value='8'>8</option>
                      <option value='9'>9</option>
                      <option value='10'>10</option>
                    </select>{' '}
                    <br />
                    <label>Comment: </label>
                    <input type='text' name='comment' ref={comment} />
                    <button onClick={ratePhoto} className="submit-button" style={{ marginLeft: '10px' }}>Rate!</button>
                  </div>
              }
            </div>
        }
        <hr style={{ border: '1px solid black' }} />
      </>
    </div>
  );
};

export default PhotoView;
