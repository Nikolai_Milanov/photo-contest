import React, { useState, useEffect } from 'react';
import httpProvider from '../../providers/http-provider';
import ContestView from './ContestView';
import Loading from '../../Loading/Loading';
import { useHistory } from 'react-router-dom';
import NoContestsImg from '../../media/azalDDZp_700w_0.jpg';

const RateContest = (props) => {
  const [contestsPhase2, setContestsPhase2] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  const handleBack = () => {
    history.goBack();
  }

  useEffect(() => {
    httpProvider
      .get(`http://localhost:3000/contests/phase2`)
      .then((result) => {
        setTimeout(() => {
          if (!result.message) {
            setContestsPhase2(result);
          }
          setLoading(false);
        }, 500);
      })
      .catch((err) => console.log(err));
  }, []);

  if (loading) {
    return <Loading />;
}

  return (
    <div>
      <button onClick={handleBack} className="back-btn">&lt;</button>
      <h1 className="page-title">Rate Contest</h1>
      {contestsPhase2.length ? (
        <div>
          {contestsPhase2.map((contest) => {
            return (
              <div key={contest.id}>
                <ContestView key={contest.id} contest={contest} />
                <br />
              </div>
            );
          })}
        </div>
      ) : (
        <div className="no-contests-container">
          <h3>No contests found!</h3>
          <img src={NoContestsImg} alt="not-found-img"/>
        </div>
      )}
    </div>
  );
};

export default RateContest;
