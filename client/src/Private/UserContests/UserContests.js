import React, { useEffect, useState } from 'react';
import { useAuth } from '../../Auth/AuthContext';
import httpProvider from '../../providers/http-provider';
import SingleContest from '../SingleContest/SingleContest';
import Loading from '../../Loading/Loading';
import moment from 'moment';
import Countdown from '../../Countdown/Countdown';
import { useHistory } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import NoContestsImg from '../../media/a6pok7lj_700w_0.jpg';

const UserContests = () => {
    const { user } = useAuth();
    const [contests, setContests] = useState([]);
    const [loading, setLoading] = useState(true);
    const currentTime = moment();
    const history = useHistory();

    const handleBack = () => {
        history.goBack();
    }

    useEffect(() => {
        let mounted = true;
        httpProvider
            .get(`http://localhost:3000/contests/open/users/${user.sub}`)
            .then((res) => {
                if (mounted) {
                    if (res.error) {
                        alert(res.error);
                    } else {
                        setTimeout(() => {
                            if (res.foundContests.length !== 0) {
                                setContests(res.foundContests);
                                setLoading(false);
                            } else {
                                setLoading(false);
                            }
                        }, 500);
                    }
                }
            })
            .catch((err) => console.log(err));
        return () => {
            mounted = false;
        };
    }, [user.sub]);

    if (loading) {
        return <Loading />;
    }

    return (
        <div>
            <button onClick={handleBack} className="back-btn">&lt;</button>
            <h1 className="page-title">CONTESTS {user.username} CURRENTLY PARTICIPATES IN:</h1>
            <>
                {contests.length !== 0
                    ?
                    contests.map(contest => {
                        return (
                            <Card key={contest.id} className="single-contest-container">
                                {contest.endPhase1Time > currentTime.format('YYYY-MM-DDTHH:mm:ss')
                                    ?
                                    <Countdown phase1End={contest.endPhase1Time} />
                                    :
                                    <Countdown phase2End={contest.endPhase2Time} />
                                }
                                <SingleContest contest={contest} />
                            </Card>
                        )
                    })
                    :
                    <div className="no-contests-container">
                        <h3>Currently not participating in any contest.</h3>
                        <img src={NoContestsImg} alt="not-found-img"/>
                    </div>
                }
            </>
        </div>
    );
};

export default UserContests;
