import React, { useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';
import { useAuth } from '../../Auth/AuthContext';
import httpProvider from '../../providers/http-provider';
import Loading from '../../Loading/Loading';
import './UserDashboard.css';
import { useHistory } from 'react-router-dom';

const UserDashboard = () => {
    const { user } = useAuth();
    const [userPoints, setUserPoints] = useState({});
    const [loading, setLoading] = useState(true);
    const history = useHistory();

    const redirectOpenContests = () => {
        history.push("/dashboard/open-contests");
    }

    const redirectParticipation = () => {
        history.push("/dashboard/participation");
    }

    const redirectParticipationContests = () => {
        history.push("/dashboard/participation/contests");
    }

    useEffect(() => {
        let mounted = true;
        httpProvider.get(`http://localhost:3000/user/${user.sub}`)
            .then(res => {
                if (mounted) {
                    setTimeout(() => {
                        if (res.id) {
                            if (res.points < 51) {
                                const points = {
                                    points: res.points,
                                    untilNextRank: 51 - res.points,
                                    currentRank: 'Junkie'
                                }
                                setUserPoints(points);
                            } else if (res.points < 151) {
                                const points = {
                                    points: res.points,
                                    untilNextRank: 151 - res.points,
                                    currentRank: 'Enthusiast'
                                }
                                setUserPoints(points);
                            } else if (res.points < 1001) {
                                const points = {
                                    points: res.points,
                                    untilNextRank: 1001 - res.points,
                                    currentRank: 'Master'
                                }
                                setUserPoints(points);
                            } else {
                                const points = {
                                    points: res.points,
                                    untilNextRank: 0,
                                    currentRank: 'Wise and Benevolent Photo Dictator'
                                }
                                setUserPoints(points);
                            }
                        } else {
                            alert(res.message);
                        }
                        setLoading(false);
                    }, 500);
                }
            })
            .catch(err => console.log(err));

        return () => {
            mounted = false;
        }
    }, [user.sub])

    if (loading) {
        return <Loading />
    }

    return (
        <div>
            <div>
                <h1 className="page-title">{user.username}'s DASHBOARD</h1>
                <div className="user-info">
                    <h4>{user.username}'s POINTS: {userPoints.points}</h4>
                    <h4>Current rank: {userPoints.currentRank}</h4>
                    <h4>Points until next rank: {userPoints.untilNextRank}</h4>
                </div>
            </div>
            <div className="nav-pages">
                <Card onClick={redirectOpenContests}>
                    <Card.Body>
                        <Card.Title>Open contests</Card.Title>
                    </Card.Body>
                </Card>
                <Card onClick={redirectParticipation}>
                    <Card.Body>
                        <Card.Title>{user.username}'s contests</Card.Title>
                    </Card.Body>
                </Card>
                <Card onClick={redirectParticipationContests}>
                    <Card.Body>
                        <Card.Title>Ended contests</Card.Title>
                    </Card.Body>
                </Card>
            </div>
        </div>
    )
}

export default UserDashboard;