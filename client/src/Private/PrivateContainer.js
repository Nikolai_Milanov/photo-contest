import React from 'react'
import Header from '../Header/Header';
import './PrivateContainer.css';

const PrivateContainer = (props) => {
    return (
        <div>
            <Header />
            <div className="main-private-container">
                <div className="private-container">
                    {props.children}
                </div>
            </div>
        </div>
    )
}

export default PrivateContainer;