import React, { useState, useEffect } from 'react';
import moment, { duration } from 'moment';

const Countdown = (props) => {
    const [timer, setTimer] = useState({ days: 0, hours: 0, mins: 0, secs: 0 });

    const addZeros = (value) => {
        let newValue = String(value);
        while (newValue.length < 2) {
            newValue = `0${newValue}`;
        }
        return newValue;
    }

    const setCountdown = () => {
        let phase1End;
        let phase2End;
        let clockDuration;
        const today = moment();

        if (props.phase1End) {
            phase1End = moment(props.phase1End);
            clockDuration = duration(phase1End.diff(today));
        } else if (props.phase2End) {
            phase2End = moment(props.phase2End);
            clockDuration = duration(phase2End.diff(today));
        }

        const days = Math.floor(clockDuration.asDays());
        const hours = Math.floor(clockDuration.hours());
        const mins = Math.floor(clockDuration.minutes());
        const secs = Math.floor(clockDuration.seconds());

        setTimer({ days, hours, mins, secs });
    }

    useEffect(() => {
        setCountdown();
        const interval = setInterval(() => {
            setCountdown();
        }, 1000);

        return () => {
            clearInterval(interval);
        }
    }, []);

    return (
        <div className="countdown">
            {props.phase1End
                ?
                <h3>Phase 1 ends in: </h3>
                :
                <h3>Phase 2 ends in: </h3>
            }
            {Object.keys(timer).map((key) => {
                return (
                    <span key={key}>
                        <span className="countdown-number">
                            {addZeros(timer[key])}
                        </span>
                        <span className="countdown-label">
                            {key}
                        </span>
                    </span>
                )
            })}
        </div>
    )
}

export default Countdown;