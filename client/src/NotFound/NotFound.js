import React from 'react';
import notFound from '../media/f1af08469a6bedc648549e3874661980.jpg';
import './NotFound.css';

const NotFound = () => {
    return (
        <div className="not-found-container">
            <div className="inner-not-found-container">
                <h1 id="not-found-title">PAGE NOT FOUND!</h1>
                <img src={notFound} alt="not-found" id="not-found-img" />
            </div>
        </div>
    )
}

export default NotFound;