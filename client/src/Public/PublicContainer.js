import React from 'react'
import Header from '../Header/Header';
import './PublicContainer.css';

const PublicContainer = (props) => {


    return (
        <div>
            <Header user={props.user} />
            {props.children}
        </div>
    )
}

export default PublicContainer;
