import React from 'react';
import './LandingPage.css';

const LandingPage = () => {

    return (
        <div className="landing-page-container">
            <div className="title-container">
                <h3>PHOTO WIZARDS CONTEST</h3>
            </div>
            <div className="quote-container">
                <blockquote>
                    "If you had one shot,<br />
                or one opportunity,<br />
                to seize everything you ever wanted,<br />
                in one moment<br />
                would you capture it,<br />
                or just let it slip?"<br />
                    <div className="quote-author">
                        <span>
                            -Eminem
                        </span>
                    </div>
                </blockquote>
            </div>
        </div>
    )
}

export default LandingPage;
