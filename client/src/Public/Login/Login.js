import React, { useRef } from 'react'
import { Form, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { useAuth } from '../../Auth/AuthContext';
import httpProvider from '../../providers/http-provider';
import tokenProvider from '../../providers/token-provider';
import decode from 'jwt-decode';
import { Container } from 'react-bootstrap';
import Swal from 'sweetalert2';

const Login = (props) => {
    const { setUser } = useAuth();
    const redirect = () => props.history.push('/dashboard');
    const username = useRef(null);
    const password = useRef(null);

    const loginValidation = (ev) => {
        ev.preventDefault();

        const userData = {
            username: username.current.value,
            password: password.current.value
        };

        return httpProvider.post('http://localhost:3000/session', userData)
            .then(res => {
                if (res.token) {
                    tokenProvider.setToken(res.token);
                    const user = decode(res.token);
                    setUser(user);
                    redirect();
                } else {
                    Swal.fire({ text: res.message });
                }
            })
            .catch(err => console.log(err));
    }

    return (
        <Container className="main-form-container">
            <Form className="form-container">
                <Form.Group controlId="formBasicText">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" placeholder="Enter username" ref={username} />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Enter password" ref={password} />
                </Form.Group>
                <Button variant="primary" type="submit" className="btn-block" onClick={loginValidation}>
                    Login
                </Button>
                <p>Don't have an account? <NavLink to="/register"><span>Register here</span></NavLink></p>
            </Form>
        </Container>
    )
}

export default Login;