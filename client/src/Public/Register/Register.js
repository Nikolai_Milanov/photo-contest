import React, { useRef } from 'react'
import { Form, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import httpProvider from '../../providers/http-provider';
import { Container } from 'react-bootstrap';
import Swal from 'sweetalert2';

const Register = (props) => {
    const username = useRef();
    const firstName = useRef();
    const lastName = useRef();
    const password = useRef();

    const redirect = () => props.history.push('/session');

    const registerValidation = (ev) => {
        ev.preventDefault();

        const userData = {
            username: username.current.value,
            firstName: firstName.current.value,
            lastName: lastName.current.value,
            password: password.current.value
        }

        return httpProvider.post('http://localhost:3000/register', userData)
            .then(res => {
                if (!res.user) {
                    Swal.fire({ text: res.message });
                } else {
                    Swal.fire({ text: 'User successfully created!' });
                    redirect();
                }
            })
            .catch(err => console.log(err));
    }

    return (
        <Container className="main-form-container">
            <Form className="form-container">
                <Form.Group controlId="formBasicUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" placeholder="Enter username" ref={username} />
                </Form.Group>
                <Form.Group controlId="formBasicFirstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter first name" ref={firstName} />
                </Form.Group>
                <Form.Group controlId="formBasicLastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter last name" ref={lastName} />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Enter password" ref={password} />
                </Form.Group>
                <Button variant="primary" type="submit" className="btn-block" onClick={registerValidation}>
                    Register
                </Button>
                <p className="form-paragraph">Already have an account? <NavLink to="/session"><span>Login</span></NavLink></p>
            </Form>
        </Container>
    )
}

export default Register;