import React, { useState } from 'react';
import './App.css';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import PublicContainer from './Public/PublicContainer';
import PrivateContainer from './Private/PrivateContainer';
import NotFound from './NotFound/NotFound';
import Login from './Public/Login/Login';
import Register from './Public/Register/Register';
import LandingPage from './Public/LandingPage/LandingPage';
import UserDashboard from './Private/UserDashboard/UserDashboard';
import OrganiserDashboard from './Private/OrganiserDashboard/OrganiserDashboard';
import CreateContest from './Private/CreateContest/CreateContest';
import ContestsPhase1 from './Private/ContestPhases/ContestsPhase1/ContestsPhase1';
import ContestsPhase2 from './Private/ContestPhases/ContestsPhase2/ContestsPhase2';
import ContestsFinished from './Private/ContestPhases/ContestsFinished/ContestsFinished';
import PhotoJunkies from './Private/PhotoJunkies/PhotoJunkies';
import RateContest from './Private/RateContest/RateContest';
import OpenContests from './Private/OpenContests/OpenContests';
import UserContests from './Private/UserContests/UserContests';
import tokenProvider from './providers/token-provider';
import decode from 'jwt-decode';
import AuthContext from './Auth/AuthContext';
import UserEndedContests from './Private/UserEndedContests/UserEndedContests';
import SingleContest from './Private/SingleContest/SingleContest';

const App = () => {
  const token = tokenProvider.getToken();
  const [user, setUser] = useState(token ? decode(token) : null);

  return (
    <div className="App">
      <AuthContext.Provider value={{ user: user, setUser: setUser }} >
        <BrowserRouter>
          {user
            ?
            <PrivateContainer user={user}>
              <Switch>
                <Redirect path="/" exact to="/dashboard" />
                {user.role === 'user'
                  ?
                  <Switch>
                    <Route path="/dashboard" exact component={UserDashboard} />
                    <Route path="/dashboard/open-contests" exact component={OpenContests} />
                    <Route path="/dashboard/open-contests/contest" exact component={SingleContest} />
                    <Route path="/dashboard/participation" exact component={UserContests} />
                    <Route path="/dashboard/participation/contests" exact component={UserEndedContests} />
                    <Route path="*" component={NotFound} />
                  </Switch>
                  :
                  <Switch>
                    <Route path="/dashboard" exact component={OrganiserDashboard} />
                    <Route path="/dashboard/contests/create" exact component={CreateContest} />
                    <Route path="/dashboard/contests/phase1" exact component={ContestsPhase1} />
                    <Route path="/dashboard/contests/phase2" exact component={ContestsPhase2} />
                    <Route path="/dashboard/contests/finished" exact component={ContestsFinished} />
                    <Route path="/dashboard/users" exact component={PhotoJunkies} />
                    <Route path="/dashboard/contests/ratecontest" exact component={RateContest} />
                    <Route path="*" component={NotFound} />
                  </Switch>
                }
              </Switch>
            </PrivateContainer>
            :
            <PublicContainer user={user}>
              <Switch>
                <Route path="/" exact component={LandingPage} />
                <Route path="/session" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="*" component={NotFound} />
              </Switch>
            </PublicContainer>
          }
        </BrowserRouter>
      </AuthContext.Provider>
    </div >
  );
}

export default App;
